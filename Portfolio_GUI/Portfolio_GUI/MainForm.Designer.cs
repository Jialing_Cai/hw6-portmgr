﻿using Portfolio_Manager;

namespace Portfolio_GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instrumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interestRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historicalPriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshTradesFromDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priceBookUsingSimulationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priceAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rateAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVolatility = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gridTotals = new System.Windows.Forms.DataGridView();
            this.totalPnLDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDeltaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalGammaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalVegaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalThetaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalRhoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.totalsDataSet = new Portfolio_GUI.TotalsDataSet();
            this.label3 = new System.Windows.Forms.Label();
            this.gridAllTrades = new System.Windows.Forms.DataGridView();
            this.tradesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.mainFormDataSet = new Portfolio_GUI.MainFormDataSet();
            this.tradesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.errorProviderVol = new System.Windows.Forms.ErrorProvider(this.components);
            this.totalsTableAdapter = new Portfolio_GUI.TotalsDataSetTableAdapters.TotalsTableAdapter();
            this.tradesTableAdapter = new Portfolio_GUI.MainFormDataSetTableAdapters.TradesTableAdapter();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAllTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFormDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderVol)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Location = new System.Drawing.Point(0, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1002, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.analysisToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1002, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem1,
            this.toolStripSeparator1,
            this.refreshTradesFromDatabaseToolStripMenuItem,
            this.priceBookUsingSimulationToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.newToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem1
            // 
            this.newToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instrumentTypeToolStripMenuItem,
            this.instrumentToolStripMenuItem,
            this.tradeToolStripMenuItem,
            this.interestRateToolStripMenuItem,
            this.historicalPriceToolStripMenuItem});
            this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
            this.newToolStripMenuItem1.Size = new System.Drawing.Size(283, 26);
            this.newToolStripMenuItem1.Text = "New...";
            // 
            // instrumentTypeToolStripMenuItem
            // 
            this.instrumentTypeToolStripMenuItem.Name = "instrumentTypeToolStripMenuItem";
            this.instrumentTypeToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.instrumentTypeToolStripMenuItem.Text = "Instrument Type";
            this.instrumentTypeToolStripMenuItem.Click += new System.EventHandler(this.instrumentTypeToolStripMenuItem_Click);
            // 
            // instrumentToolStripMenuItem
            // 
            this.instrumentToolStripMenuItem.Name = "instrumentToolStripMenuItem";
            this.instrumentToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.instrumentToolStripMenuItem.Text = "Instrument";
            this.instrumentToolStripMenuItem.Click += new System.EventHandler(this.instrumentToolStripMenuItem_Click);
            // 
            // tradeToolStripMenuItem
            // 
            this.tradeToolStripMenuItem.Name = "tradeToolStripMenuItem";
            this.tradeToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.tradeToolStripMenuItem.Text = "Trade";
            this.tradeToolStripMenuItem.Click += new System.EventHandler(this.tradeToolStripMenuItem_Click);
            // 
            // interestRateToolStripMenuItem
            // 
            this.interestRateToolStripMenuItem.Name = "interestRateToolStripMenuItem";
            this.interestRateToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.interestRateToolStripMenuItem.Text = "Interest Rate";
            this.interestRateToolStripMenuItem.Click += new System.EventHandler(this.interestRateToolStripMenuItem_Click);
            // 
            // historicalPriceToolStripMenuItem
            // 
            this.historicalPriceToolStripMenuItem.Name = "historicalPriceToolStripMenuItem";
            this.historicalPriceToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.historicalPriceToolStripMenuItem.Text = "Historical Price";
            this.historicalPriceToolStripMenuItem.Click += new System.EventHandler(this.historicalPriceToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(280, 6);
            // 
            // refreshTradesFromDatabaseToolStripMenuItem
            // 
            this.refreshTradesFromDatabaseToolStripMenuItem.Name = "refreshTradesFromDatabaseToolStripMenuItem";
            this.refreshTradesFromDatabaseToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.refreshTradesFromDatabaseToolStripMenuItem.Text = "Refresh Trades from Database";
            this.refreshTradesFromDatabaseToolStripMenuItem.Click += new System.EventHandler(this.refreshTradesFromDatabaseToolStripMenuItem_Click);
            // 
            // priceBookUsingSimulationToolStripMenuItem
            // 
            this.priceBookUsingSimulationToolStripMenuItem.Name = "priceBookUsingSimulationToolStripMenuItem";
            this.priceBookUsingSimulationToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.priceBookUsingSimulationToolStripMenuItem.Text = "Price Book Using Simulation";
            this.priceBookUsingSimulationToolStripMenuItem.Click += new System.EventHandler(this.priceBookUsingSimulationToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(280, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // analysisToolStripMenuItem
            // 
            this.analysisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.priceAnalysisToolStripMenuItem,
            this.rateAnalysisToolStripMenuItem});
            this.analysisToolStripMenuItem.Name = "analysisToolStripMenuItem";
            this.analysisToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.analysisToolStripMenuItem.Text = "&Analysis";
            // 
            // priceAnalysisToolStripMenuItem
            // 
            this.priceAnalysisToolStripMenuItem.Name = "priceAnalysisToolStripMenuItem";
            this.priceAnalysisToolStripMenuItem.Size = new System.Drawing.Size(173, 26);
            this.priceAnalysisToolStripMenuItem.Text = "Price Analysis";
            this.priceAnalysisToolStripMenuItem.Click += new System.EventHandler(this.priceAnalysisToolStripMenuItem_Click);
            // 
            // rateAnalysisToolStripMenuItem
            // 
            this.rateAnalysisToolStripMenuItem.Name = "rateAnalysisToolStripMenuItem";
            this.rateAnalysisToolStripMenuItem.Size = new System.Drawing.Size(173, 26);
            this.rateAnalysisToolStripMenuItem.Text = "Rate Analysis";
            this.rateAnalysisToolStripMenuItem.Click += new System.EventHandler(this.rateAnalysisToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simulatorToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.settingsToolStripMenuItem.Text = "&Settings";
            // 
            // simulatorToolStripMenuItem
            // 
            this.simulatorToolStripMenuItem.Name = "simulatorToolStripMenuItem";
            this.simulatorToolStripMenuItem.Size = new System.Drawing.Size(157, 26);
            this.simulatorToolStripMenuItem.Text = "Simulator...";
            this.simulatorToolStripMenuItem.Click += new System.EventHandler(this.simulatorToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pricing Volatility:";
            // 
            // txtVolatility
            // 
            this.txtVolatility.Location = new System.Drawing.Point(143, 41);
            this.txtVolatility.Name = "txtVolatility";
            this.txtVolatility.Size = new System.Drawing.Size(112, 22);
            this.txtVolatility.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(389, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Totals (Select the rows from \"All Trades\" below to get totals):";
            // 
            // gridTotals
            // 
            this.gridTotals.AllowUserToAddRows = false;
            this.gridTotals.AllowUserToDeleteRows = false;
            this.gridTotals.AutoGenerateColumns = false;
            this.gridTotals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTotals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.totalPnLDataGridViewTextBoxColumn,
            this.totalDeltaDataGridViewTextBoxColumn,
            this.totalGammaDataGridViewTextBoxColumn,
            this.totalVegaDataGridViewTextBoxColumn,
            this.totalThetaDataGridViewTextBoxColumn,
            this.totalRhoDataGridViewTextBoxColumn});
            this.gridTotals.DataSource = this.totalsBindingSource;
            this.gridTotals.Location = new System.Drawing.Point(29, 96);
            this.gridTotals.Name = "gridTotals";
            this.gridTotals.RowTemplate.Height = 24;
            this.gridTotals.Size = new System.Drawing.Size(944, 103);
            this.gridTotals.TabIndex = 5;
            // 
            // totalPnLDataGridViewTextBoxColumn
            // 
            this.totalPnLDataGridViewTextBoxColumn.DataPropertyName = "TotalPnL";
            this.totalPnLDataGridViewTextBoxColumn.HeaderText = "TotalPnL";
            this.totalPnLDataGridViewTextBoxColumn.Name = "totalPnLDataGridViewTextBoxColumn";
            // 
            // totalDeltaDataGridViewTextBoxColumn
            // 
            this.totalDeltaDataGridViewTextBoxColumn.DataPropertyName = "TotalDelta";
            this.totalDeltaDataGridViewTextBoxColumn.HeaderText = "TotalDelta";
            this.totalDeltaDataGridViewTextBoxColumn.Name = "totalDeltaDataGridViewTextBoxColumn";
            // 
            // totalGammaDataGridViewTextBoxColumn
            // 
            this.totalGammaDataGridViewTextBoxColumn.DataPropertyName = "TotalGamma";
            this.totalGammaDataGridViewTextBoxColumn.HeaderText = "TotalGamma";
            this.totalGammaDataGridViewTextBoxColumn.Name = "totalGammaDataGridViewTextBoxColumn";
            // 
            // totalVegaDataGridViewTextBoxColumn
            // 
            this.totalVegaDataGridViewTextBoxColumn.DataPropertyName = "TotalVega";
            this.totalVegaDataGridViewTextBoxColumn.HeaderText = "TotalVega";
            this.totalVegaDataGridViewTextBoxColumn.Name = "totalVegaDataGridViewTextBoxColumn";
            // 
            // totalThetaDataGridViewTextBoxColumn
            // 
            this.totalThetaDataGridViewTextBoxColumn.DataPropertyName = "TotalTheta";
            this.totalThetaDataGridViewTextBoxColumn.HeaderText = "TotalTheta";
            this.totalThetaDataGridViewTextBoxColumn.Name = "totalThetaDataGridViewTextBoxColumn";
            // 
            // totalRhoDataGridViewTextBoxColumn
            // 
            this.totalRhoDataGridViewTextBoxColumn.DataPropertyName = "TotalRho";
            this.totalRhoDataGridViewTextBoxColumn.HeaderText = "TotalRho";
            this.totalRhoDataGridViewTextBoxColumn.Name = "totalRhoDataGridViewTextBoxColumn";
            // 
            // totalsBindingSource
            // 
            this.totalsBindingSource.DataMember = "Totals";
            this.totalsBindingSource.DataSource = this.totalsDataSet;
            // 
            // totalsDataSet
            // 
            this.totalsDataSet.DataSetName = "TotalsDataSet";
            this.totalsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "All Trades:";
            // 
            // gridAllTrades
            // 
            this.gridAllTrades.AllowUserToAddRows = false;
            this.gridAllTrades.AllowUserToDeleteRows = false;
            this.gridAllTrades.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridAllTrades.GridColor = System.Drawing.SystemColors.Control;
            this.gridAllTrades.Location = new System.Drawing.Point(29, 243);
            this.gridAllTrades.Name = "gridAllTrades";
            this.gridAllTrades.RowTemplate.Height = 24;
            this.gridAllTrades.Size = new System.Drawing.Size(944, 195);
            this.gridAllTrades.TabIndex = 7;
            this.gridAllTrades.SelectionChanged += new System.EventHandler(this.gridAllTrades_SelectionChanged);
            // 
            // tradesBindingSource1
            // 
            this.tradesBindingSource1.DataMember = "Trades";
            this.tradesBindingSource1.DataSource = this.mainFormDataSet;
            // 
            // mainFormDataSet
            // 
            this.mainFormDataSet.DataSetName = "MainFormDataSet";
            this.mainFormDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // errorProviderVol
            // 
            this.errorProviderVol.ContainerControl = this;
            // 
            // totalsTableAdapter
            // 
            this.totalsTableAdapter.ClearBeforeFill = true;
            // 
            // tradesTableAdapter
            // 
            this.tradesTableAdapter.ClearBeforeFill = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 472);
            this.Controls.Add(this.gridAllTrades);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gridTotals);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtVolatility);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Portfolio Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAllTrades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFormDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tradesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderVol)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshTradesFromDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem priceBookUsingSimulationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem instrumentTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instrumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interestRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historicalPriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem priceAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rateAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simulatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gridTotals;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView gridAllTrades;
        public System.Windows.Forms.TextBox txtVolatility;
        private System.Windows.Forms.ErrorProvider errorProviderVol;
        private TotalsDataSet totalsDataSet;
        private System.Windows.Forms.BindingSource totalsBindingSource;
        private TotalsDataSetTableAdapters.TotalsTableAdapter totalsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalPnLDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDeltaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalGammaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalVegaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalThetaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalRhoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource tradesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn isBuyDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timestampDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn markPriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pnLDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deltaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gammaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vegaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn thetaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rhoDataGridViewTextBoxColumn;
        private MainFormDataSet mainFormDataSet;
        private System.Windows.Forms.BindingSource tradesBindingSource1;
        private MainFormDataSetTableAdapters.TradesTableAdapter tradesTableAdapter;
    }
}

