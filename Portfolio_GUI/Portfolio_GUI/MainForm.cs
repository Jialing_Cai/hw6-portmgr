﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MCSimulator;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        DataModelContainer db = new DataModelContainer();

        /// <summary>
        /// Set default DataGridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // Clear simulator-related data
            foreach (var trade in db.Trades)
            {
                var ticker = (from p in db.Instruments
                              where p.Id == trade.InstrumentId
                              select p.Ticker).FirstOrDefault();

                trade.Price = (from g in db.HistoryPrices
                               join gg in db.Instruments
                               on g.InstrumentId equals gg.Id
                               where gg.Ticker == ticker
                               orderby g.Date descending
                               select g.ClosingPrice).FirstOrDefault();

                trade.MarkPrice = trade.PnL = trade.Delta = trade.Gamma = trade.Vega = trade.Theta = trade.Rho = 0;
            }
            db.SaveChanges();
            RefreshTradeView();

            //// TODO: This line of code loads data into the 'mainFormDataSet.Trades' table. You can move, or remove it, as needed.
            //this.tradesTableAdapter.Fill(this.mainFormDataSet.Trades);

            // Add datasource to Totals-table
            // TODO: This line of code loads data into the 'totalsDataSet.Totals' table. You can move, or remove it, as needed.
            this.totalsTableAdapter.Fill(this.totalsDataSet.Totals);
            txtVolatility.Text = "0.5";
        }

        /// <summary>
        /// Load columns to AllTrade-grid
        /// </summary>
        private void RefreshTradeView()
        {
            gridAllTrades.DataSource = (from g1 in db.Trades
                                        join g2 in db.Instruments
                                        on g1.InstrumentId equals g2.Id
                                        select new { g2.OptionTypeName, g2.Ticker, g1.IsBuy, g1.Quantity, g1.Price, g1.MarkPrice, g1.PnL, g1.Delta, g1.Gamma, g1.Vega, g1.Theta, g1.Rho })
                       .OrderByDescending(g => g.Ticker).ToList();
            gridAllTrades.Columns["OptionTypeName"].HeaderText = "InstrumentName";
            gridAllTrades.Columns["Price"].HeaderText = "TradePrice";
        }

        /// <summary>
        /// Add up the chosen rows to calculate Totals
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridAllTrades_SelectionChanged(object sender, EventArgs e)
        {
            double pnl = 0, delta = 0, gamma = 0, vega = 0, theta = 0, rho = 0;

            foreach (DataGridViewRow row in gridAllTrades.SelectedRows)
            {
                pnl += Convert.ToDouble(row.Cells["PnL"].Value);
                delta += Convert.ToDouble(row.Cells["Delta"].Value);
                gamma += Convert.ToDouble(row.Cells["Gamma"].Value);
                vega += Convert.ToDouble(row.Cells["Vega"].Value);
                theta += Convert.ToDouble(row.Cells["Theta"].Value);
                rho += Convert.ToDouble(row.Cells["Rho"].Value);
            }

            foreach (var g in db.Totals) { db.Totals.Remove(g); } //Clear table first
            db.Totals.Add(new Totals
            {
                TotalPnL = pnl,
                TotalDelta = delta,
                TotalGamma = gamma,
                TotalVega = vega,
                TotalTheta = theta,
                TotalRho = rho
            });

            db.SaveChanges();
            this.totalsTableAdapter.Fill(this.totalsDataSet.Totals);
        }

        private void instrumentTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewInstrumentType instrtype = new NewInstrumentType();
            instrtype.ShowDialog();
        }

        private void instrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewInstrument instr = new NewInstrument();
            instr.ShowDialog();
        }

        private void tradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewTrade trade = new NewTrade();
            trade.ShowDialog();
        }

        private void interestRateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewIntRate intRate = new NewIntRate();
            intRate.ShowDialog();
        }

        private void historicalPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewHistPrice histPrice = new NewHistPrice();
            histPrice.ShowDialog();
        }

        private void simulatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SimulatorSetting simulatorSetting = new SimulatorSetting();
            simulatorSetting.ShowDialog();
        }

        private void priceAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PriceAnalysis priceAnalysis = new PriceAnalysis();
            priceAnalysis.ShowDialog();
        }

        private void rateAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RateAnalysis rateAnalysis = new RateAnalysis();
            rateAnalysis.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void priceBookUsingSimulationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            simulatorToolStripMenuItem_Click(sender, e); // Let user set simulator properties first

            // pick all the stocks (id)
            var stock_id = (from g in db.Instruments
                            where g.OptionTypeName.Contains("Stock")
                            select g.Id).ToList();

            try
            {
                foreach (var trade in db.Trades.ToList())
                {
                    if (stock_id.Contains(trade.InstrumentId)) //if trade stock...
                    {
                        trade.MarkPrice = (from g in db.HistoryPrices
                                           where g.InstrumentId == trade.InstrumentId
                                           orderby g.Date descending
                                           select g.ClosingPrice).FirstOrDefault(); // MarkPrice = most recent price
                        if (trade.IsBuy)
                        {
                            trade.PnL = (double)trade.Quantity * (trade.MarkPrice - trade.Price);
                            trade.Delta = (double)trade.Quantity;
                        }
                        else
                        {
                            trade.PnL = (double)-trade.Quantity * (trade.MarkPrice - trade.Price);
                            trade.Delta = (double)-trade.Quantity;
                        }
                        trade.Gamma = trade.Vega = trade.Theta = trade.Rho = 0;

                        db.SaveChanges();
                        //MessageBox.Show("stock succeed");
                        RefreshTradeView(); // update grid from db
                    }
                    else //if trade option...
                    {
                        // set all the inputs for simulator and then pass to the Price_func to simulate
                        int Cpu_cores = System.Environment.ProcessorCount;
                        double Call_Put = (from g in db.Instruments
                                           where g.Id == trade.InstrumentId
                                           select g.IsCall).FirstOrDefault() ? 1 : -1;

                        // Barrier Type
                        string Barrier_Type_name = (from g in db.Instruments
                                                    where g.Id == trade.InstrumentId
                                                    select g.BarrierTypeName).FirstOrDefault().ToString();

                        var ticker = (from p in db.Instruments
                                      where p.Id == trade.InstrumentId
                                      select p.Ticker).FirstOrDefault();

                        trade.Price = (from g in db.HistoryPrices
                                       join gg in db.Instruments
                                       on g.InstrumentId equals gg.Id
                                       where gg.Ticker == ticker
                                       orderby g.Date descending
                                       select g.ClosingPrice).FirstOrDefault(); // most recent price

                        double Underlying = trade.Price;

                        double Strike = (from g in db.Instruments
                                         where g.Id == trade.InstrumentId
                                         select g.Strike).FirstOrDefault();

                        double Vol = Convert.ToDouble(txtVolatility.Text);

                        double Tenor = (from g in db.Instruments
                                        where g.Id == trade.InstrumentId
                                        select g.Tenor).FirstOrDefault();

                        double Barrier_level = (from g in db.Instruments
                                                where g.Id == trade.InstrumentId
                                                select g.Barrier).FirstOrDefault();

                        double Rebate = (from g in db.Instruments
                                         where g.Id == trade.InstrumentId
                                         select g.Rebate).FirstOrDefault();

                        // Rate - linear interpolation
                        double R = 0;

                        // add lower-tenor and higher-tenor to each list
                        List<InterestRate> lowratelist = new List<InterestRate>();
                        List<InterestRate> highratelist = new List<InterestRate>();
                        foreach (var g in db.InterestRates)
                        {
                            if (g.Tenor == Tenor) { R = g.Rate; }
                            else
                            {
                                if (g.Tenor < Tenor) { lowratelist.Add(g); }
                                else if (g.Tenor > Tenor) { highratelist.Add(g); }
                            }
                        }

                        if (R == 0) // if not exist the same tenor in db, then do linear interpolation
                        {
                            if (lowratelist.Count == 0)
                            {
                                double multiplier = Tenor / Convert.ToDouble(highratelist.Min(x => x.Tenor));
                                R = multiplier * Convert.ToDouble(highratelist.OrderBy(x => x.Tenor).Select(x => x.Rate).FirstOrDefault());
                            }
                            else if (highratelist.Count == 0)
                            {
                                double multiplier = Tenor / Convert.ToDouble(lowratelist.Max(x => x.Tenor));
                                R = Convert.ToDouble(lowratelist.OrderByDescending(x => x.Tenor).Select(x => x.Rate).FirstOrDefault());
                            }
                            else // linear interpolation
                            {
                                double rate_diff = Convert.ToDouble(highratelist.OrderBy(x => x.Tenor).Select(x => x.Rate).FirstOrDefault()) - Convert.ToDouble(lowratelist.OrderByDescending(x => x.Tenor).Select(x => x.Rate).FirstOrDefault());
                                double tenor_diff = Convert.ToDouble(highratelist.Min(x => x.Tenor)) - Convert.ToDouble(lowratelist.Max(x => x.Tenor));
                                R = rate_diff / tenor_diff * (Tenor - Convert.ToDouble(lowratelist.Max(x => x.Tenor))) + Convert.ToDouble(lowratelist.OrderByDescending(x => x.Tenor).Select(x => x.Rate).FirstOrDefault());
                            }

                            //MessageBox.Show(trade.InstrumentId + ": " + R.ToString());
                        }

                        //Option type
                        string Option_type = (from g1 in db.Instruments
                                              where g1.Id == trade.InstrumentId
                                              join g2 in db.InstTypes
                                              on g1.InstTypeId equals g2.Id
                                              select g2.TypeName).FirstOrDefault().ToString();
                        
                        // pass all the inpouts to simulator
                        double[] result = price_all(Cpu_cores, Underlying, Strike, Tenor, R, Vol, Barrier_level, Rebate, Call_Put, Option_type, Barrier_Type_name);

                        // pass all the outputs to db
                        if (trade.IsBuy)
                        {
                            trade.MarkPrice = result[0];
                            trade.PnL = (double)trade.Quantity * (result[0] - trade.Price);
                            trade.Delta = (double)trade.Quantity * result[1];
                            trade.Gamma = (double)trade.Quantity * result[2];
                            trade.Vega = (double)trade.Quantity * result[3];
                            trade.Theta = (double)trade.Quantity * result[4];
                            trade.Rho = (double)trade.Quantity * result[5];
                        }
                        else
                        {
                            trade.MarkPrice = result[0];
                            trade.PnL = (double)-trade.Quantity * (result[0] - trade.Price);
                            trade.Delta = (double)-trade.Quantity * result[1];
                            trade.Gamma = (double)-trade.Quantity * result[2];
                            trade.Vega = (double)-trade.Quantity * result[3];
                            trade.Theta = (double)-trade.Quantity * result[4];
                            trade.Rho = (double)-trade.Quantity * result[5];
                        }

                        db.SaveChanges();
                        RefreshTradeView();
                        //MessageBox.Show("Option pricing succeeds!");

                    } // end option pricing
                } // end trade loop
                MessageBox.Show("Pricing succeeds!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void refreshTradesFromDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshTradeView();
        }

        private double[] price_all(int core, double s, double k, double t, double r, double vol, double bar, double reb, double call_put, string option_type, string barrier_type_name)
        {
            double[] result = new double[6];

            IO.Cpu_cores = core;
            IO.Underlying = s;
            IO.Strike = k;
            IO.Tenor = t;
            IO.R = r;
            IO.Vol = vol;
            IO.Barrier = bar;
            IO.Rebate = reb;
            IO.Call_Put = call_put;

            switch (barrier_type_name)
            {
                case "Up and In":
                    IO.Barrier_Type = 1;
                    break;
                case "Up and Out":
                    IO.Barrier_Type = 2;
                    break;
                case "Down and In":
                    IO.Barrier_Type = 3;
                    break;
                case "Down and Out":
                    IO.Barrier_Type = 4;
                    break;
            }

            switch (option_type)
            {
                case "European Option":
                    European euro = new European();
                    Thread thrdShd1 = new Thread(new ThreadStart(euro.Scheduler));
                    thrdShd1.Start(); // calculate all outputs
                    break;
                case "Asian Option":
                    Asian asian = new Asian();
                    Thread thrdShd2 = new Thread(new ThreadStart(asian.Scheduler));
                    thrdShd2.Start(); // calculate all outputs
                    break;
                case "Barrier Option":
                    MCSimulator.Barrier barrier = new MCSimulator.Barrier();
                    Thread thrdShd3 = new Thread(new ThreadStart(barrier.Scheduler));
                    thrdShd3.Start(); // calculate all outputs
                    break;
                case "Digital Option":
                    Digital digital = new Digital();
                    Thread thrdShd4 = new Thread(new ThreadStart(digital.Scheduler));
                    thrdShd4.Start(); // calculate all outputs
                    break;
                case "Lookback Option":
                    Lookback lookback = new Lookback();
                    Thread thrdShd5 = new Thread(new ThreadStart(lookback.Scheduler));
                    thrdShd5.Start(); // calculate all outputs
                    break;
                case "Range":
                    Range range = new Range();
                    IO.Strike = IO.Underlying;
                    Thread thrdShd6 = new Thread(new ThreadStart(range.Scheduler));
                    thrdShd6.Start(); // calculate all outputs
                    break;
            }

            result[0] = IO.Price;
            result[1] = IO.Delta;
            result[2] = IO.Gamma;
            result[3] = IO.Vega;
            result[4] = IO.Theta;
            result[5] = IO.Rho;
            return result;
        }

    }
}
