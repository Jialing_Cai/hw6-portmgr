﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class NewInstrument : Form
    {
        DataModelContainer db = new DataModelContainer();

        public NewInstrument()
        {
            InitializeComponent();
        }

        private void NewInstrument_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'instrumentsDataSet.Instruments' table. You can move, or remove it, as needed.
            this.instrumentsTableAdapter.Fill(this.instrumentsDataSet.Instruments);

            foreach (var g in db.InstTypes) { cmbInstrType.Items.Add(g.TypeName); }

            foreach (var g in db.Instruments)
            {
                if (g.OptionTypeName.Contains("Stock")) { cmbUnderlying.Items.Add(g.Ticker); }
            }

            cmbInstrType.SelectedIndex = 0;
            grpStock.Enabled = true;
            btnSaveStock.Enabled = true;

            grpOption.Enabled = false;
            grpBarrier.Enabled = false;
            grpDigital.Enabled = false;
            btnSaveOption.Enabled = false;
        }

        private void btnSaveStock_Click(object sender, EventArgs e)
        {
            try
            {
                db.Instruments.Add(new Instrument
                {
                    Ticker = txtTicker.Text,
                    CompanyName = txtCompanyName.Text,
                    Exchange = txtExchange.Text,
                    OptionTypeName = cmbInstrType.Text + " on " + txtTicker.Text,
                    IsCall = true,
                    Tenor = 0,
                    Strike = 0,
                    Barrier = 0,
                    Rebate = 0,
                    BarrierTypeName = "None",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == cmbInstrType.Text
                                  select insttypes.Id).FirstOrDefault()
                });

                db.SaveChanges();
                NewInstrument_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSaveOption_Click(object sender, EventArgs e)
        {
            MainForm mainForm = new MainForm();
            try
            {
                db.Instruments.Add(new Instrument
                {
                    Ticker = cmbUnderlying.Text,
                    CompanyName = (from stock in db.Instruments
                                   where stock.Ticker == cmbUnderlying.Text
                                   select stock.CompanyName).FirstOrDefault(),
                    Exchange = (from stock in db.Instruments
                                where stock.Ticker == cmbUnderlying.Text
                                select stock.Exchange).FirstOrDefault(),
                    OptionTypeName = cmbCallPut.Text + " " + cmbInstrType.Text + " on " + cmbUnderlying.Text,
                    IsCall = (cmbCallPut.Text == "Call") ? true : false,
                    Tenor = Convert.ToDouble(txtTenor.Text),
                    Strike = Convert.ToDouble(txtStrike.Text),
                    Barrier = Convert.ToDouble(txtBarrier.Text),
                    Rebate = Convert.ToDouble(txtRebate.Text),
                    BarrierTypeName = cmbBarrierType.Text,
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == cmbInstrType.Text
                                  select insttypes.Id).FirstOrDefault()
                });

                db.SaveChanges();
                NewInstrument_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cmbInstrType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbInstrType.Text)
            {
                case "Stock":
                    grpStock.Enabled = true;
                    btnSaveStock.Enabled = true;
                    btnSaveOption.Enabled = false;
                    grpOption.Enabled = false;
                    grpBarrier.Enabled = false;
                    grpDigital.Enabled = false;
                    break;
                case "Barrier Option":
                    grpStock.Enabled = false;
                    btnSaveStock.Enabled = false;
                    btnSaveOption.Enabled = true;
                    grpOption.Enabled = true;
                    grpBarrier.Enabled = true;
                    grpDigital.Enabled = false;
                    txtRebate.Text = "0";
                    break;
                case "Digital Option":
                    grpStock.Enabled = false;
                    btnSaveStock.Enabled = false;
                    btnSaveOption.Enabled = true;
                    grpOption.Enabled = true;
                    grpBarrier.Enabled = false;
                    grpDigital.Enabled = true;
                    txtBarrier.Text = "0";
                    cmbBarrierType.Text = "None";
                    break;
                default:
                    grpStock.Enabled = false;
                    btnSaveStock.Enabled = false;
                    btnSaveOption.Enabled = true;
                    grpOption.Enabled = true;
                    grpBarrier.Enabled = false;
                    grpDigital.Enabled = false;
                    txtBarrier.Text = "0";
                    cmbBarrierType.SelectedIndex = 0;
                    txtRebate.Text = "0";
                    break;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            NewInstrument_Load(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in gridInstrument.SelectedRows)
                {
                    gridInstrument.Rows.Remove(row);
                }

                this.instrumentsTableAdapter.Update(this.instrumentsDataSet.Instruments);
                NewInstrument_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
