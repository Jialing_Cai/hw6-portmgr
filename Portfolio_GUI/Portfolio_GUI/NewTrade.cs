﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class NewTrade : Form
    {
        public NewTrade()
        {
            InitializeComponent();
        }

        DataModelContainer db = new DataModelContainer();

        private void NewTrade_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'tradeDataSet.Trades' table. You can move, or remove it, as needed.
            this.tradesTableAdapter.Fill(this.tradeDataSet.Trades);

            dateTimePicker1.MaxDate = DateTime.Today;
            foreach (var g in db.Instruments) { cmbInstrument.Items.Add(g.OptionTypeName); }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db.Trades.Add(new Trade
                {
                    IsBuy = radioBuy.Checked ? true : false,
                    Quantity = Convert.ToInt32(txtQuantity.Text),
                    Price = Convert.ToDouble(txtPrice.Text),
                    Timestamp = dateTimePicker1.Value,
                    InstrumentId = (from g in db.Instruments
                                    where g.OptionTypeName == cmbInstrument.Text
                                    select g.Id).FirstOrDefault(),
                    MarkPrice = 0,
                    PnL = 0,
                    Delta = 0,
                    Gamma = 0,
                    Vega = 0,
                    Theta = 0,
                    Rho = 0
                });

                db.SaveChanges();
                this.tradesTableAdapter.Fill(this.tradeDataSet.Trades);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.tradesTableAdapter.Fill(this.tradeDataSet.Trades);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in gridTrade.SelectedRows)
                {
                    gridTrade.Rows.Remove(row);
                }

                this.tradesTableAdapter.Update(this.tradeDataSet.Trades);
                this.tradesTableAdapter.Fill(this.tradeDataSet.Trades);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
