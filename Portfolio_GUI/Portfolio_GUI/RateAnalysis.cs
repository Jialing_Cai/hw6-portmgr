﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class RateAnalysis : Form
    {
        public RateAnalysis()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        DataModelContainer db = new DataModelContainer();

        private void RateAnalysis_Load(object sender, EventArgs e)
        {
            gridRateAnalysis.DataSource = (from g in db.InterestRates
                                           select new { g.Tenor, g.Rate }).ToList();

            chart1.Series[0].XValueMember = "Tenor";
            chart1.ChartAreas[0].AxisX.Title = "Tenor";
            chart1.Series[0].YValueMembers = "Rate";
            chart1.ChartAreas[0].AxisY.Title = "Rate";
            chart1.DataSource = gridRateAnalysis.DataSource;
            chart1.DataBind();
        }
    }
}
