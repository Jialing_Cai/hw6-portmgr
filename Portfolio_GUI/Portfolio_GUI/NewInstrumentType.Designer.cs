﻿namespace Portfolio_GUI
{
    partial class NewInstrumentType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.radioStock = new System.Windows.Forms.RadioButton();
            this.radioLookback = new System.Windows.Forms.RadioButton();
            this.radioRange = new System.Windows.Forms.RadioButton();
            this.radioDigital = new System.Windows.Forms.RadioButton();
            this.radioBarrier = new System.Windows.Forms.RadioButton();
            this.radioAsian = new System.Windows.Forms.RadioButton();
            this.radioEuro = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.gridInstType = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.instTypeDataSet = new Portfolio_GUI.InstTypeDataSet();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.instTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.instTypesTableAdapter = new Portfolio_GUI.InstTypeDataSetTableAdapters.InstTypesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.gridInstType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instTypeDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Instrument Type:";
            // 
            // radioStock
            // 
            this.radioStock.AutoSize = true;
            this.radioStock.Checked = true;
            this.radioStock.Location = new System.Drawing.Point(169, 14);
            this.radioStock.Name = "radioStock";
            this.radioStock.Size = new System.Drawing.Size(64, 21);
            this.radioStock.TabIndex = 1;
            this.radioStock.TabStop = true;
            this.radioStock.Text = "Stock";
            this.radioStock.UseVisualStyleBackColor = true;
            // 
            // radioLookback
            // 
            this.radioLookback.AutoSize = true;
            this.radioLookback.Location = new System.Drawing.Point(169, 149);
            this.radioLookback.Name = "radioLookback";
            this.radioLookback.Size = new System.Drawing.Size(136, 21);
            this.radioLookback.TabIndex = 2;
            this.radioLookback.Text = "Lookback Option";
            this.radioLookback.UseVisualStyleBackColor = true;
            // 
            // radioRange
            // 
            this.radioRange.AutoSize = true;
            this.radioRange.Location = new System.Drawing.Point(169, 176);
            this.radioRange.Name = "radioRange";
            this.radioRange.Size = new System.Drawing.Size(117, 21);
            this.radioRange.TabIndex = 3;
            this.radioRange.Text = "Range Option";
            this.radioRange.UseVisualStyleBackColor = true;
            // 
            // radioDigital
            // 
            this.radioDigital.AutoSize = true;
            this.radioDigital.Location = new System.Drawing.Point(169, 122);
            this.radioDigital.Name = "radioDigital";
            this.radioDigital.Size = new System.Drawing.Size(114, 21);
            this.radioDigital.TabIndex = 4;
            this.radioDigital.Text = "Digital Option";
            this.radioDigital.UseVisualStyleBackColor = true;
            // 
            // radioBarrier
            // 
            this.radioBarrier.AutoSize = true;
            this.radioBarrier.Location = new System.Drawing.Point(169, 95);
            this.radioBarrier.Name = "radioBarrier";
            this.radioBarrier.Size = new System.Drawing.Size(118, 21);
            this.radioBarrier.TabIndex = 5;
            this.radioBarrier.Text = "Barrier Option";
            this.radioBarrier.UseVisualStyleBackColor = true;
            // 
            // radioAsian
            // 
            this.radioAsian.AutoSize = true;
            this.radioAsian.Location = new System.Drawing.Point(169, 68);
            this.radioAsian.Name = "radioAsian";
            this.radioAsian.Size = new System.Drawing.Size(110, 21);
            this.radioAsian.TabIndex = 6;
            this.radioAsian.Text = "Asian Option";
            this.radioAsian.UseVisualStyleBackColor = true;
            // 
            // radioEuro
            // 
            this.radioEuro.AutoSize = true;
            this.radioEuro.Location = new System.Drawing.Point(169, 41);
            this.radioEuro.Name = "radioEuro";
            this.radioEuro.Size = new System.Drawing.Size(137, 21);
            this.radioEuro.TabIndex = 7;
            this.radioEuro.Text = "European Option";
            this.radioEuro.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(31, 216);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(275, 43);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save to DB";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gridInstType
            // 
            this.gridInstType.AllowUserToAddRows = false;
            this.gridInstType.AllowUserToDeleteRows = false;
            this.gridInstType.AutoGenerateColumns = false;
            this.gridInstType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridInstType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.gridInstType.DataSource = this.bindingSource1;
            this.gridInstType.Location = new System.Drawing.Point(337, 45);
            this.gridInstType.Name = "gridInstType";
            this.gridInstType.RowTemplate.Height = 24;
            this.gridInstType.Size = new System.Drawing.Size(277, 214);
            this.gridInstType.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TypeName";
            this.dataGridViewTextBoxColumn2.HeaderText = "TypeName";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "InstTypes";
            this.bindingSource1.DataSource = this.instTypeDataSet;
            // 
            // instTypeDataSet
            // 
            this.instTypeDataSet.DataSetName = "InstTypeDataSet";
            this.instTypeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(447, 14);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(167, 25);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "Delete and Save";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(337, 14);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(88, 25);
            this.btnRefresh.TabIndex = 12;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // instTypeBindingSource
            // 
            this.instTypeBindingSource.DataSource = typeof(Portfolio_Manager.InstType);
            // 
            // instTypesTableAdapter
            // 
            this.instTypesTableAdapter.ClearBeforeFill = true;
            // 
            // NewInstrumentType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 275);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.gridInstType);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.radioEuro);
            this.Controls.Add(this.radioAsian);
            this.Controls.Add(this.radioBarrier);
            this.Controls.Add(this.radioDigital);
            this.Controls.Add(this.radioRange);
            this.Controls.Add(this.radioLookback);
            this.Controls.Add(this.radioStock);
            this.Controls.Add(this.label1);
            this.Name = "NewInstrumentType";
            this.Text = "New Instrument Type...";
            this.Load += new System.EventHandler(this.NewInstrumentType_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridInstType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instTypeDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instTypeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioStock;
        private System.Windows.Forms.RadioButton radioLookback;
        private System.Windows.Forms.RadioButton radioRange;
        private System.Windows.Forms.RadioButton radioDigital;
        private System.Windows.Forms.RadioButton radioBarrier;
        private System.Windows.Forms.RadioButton radioAsian;
        private System.Windows.Forms.RadioButton radioEuro;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView gridInstType;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.BindingSource instTypeBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource instTypesBindingSource;
        private InstTypeDataSet instTypeDataSet;
        private System.Windows.Forms.BindingSource bindingSource1;
        private InstTypeDataSetTableAdapters.InstTypesTableAdapter instTypesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}