﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MCSimulator;

namespace Portfolio_GUI
{
    public partial class SimulatorSetting : Form
    {
        public SimulatorSetting()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            IO.NSteps = Convert.ToInt32(txtNSteps.Text);
            IO.NSims = Convert.ToInt64(txtNSims.Text);

            // Use Antithetic or not
            IO.IsAnti = checkBx_Anti.Checked ? true : false;

            // Use Control Variate or not
            IO.IsCv = checkBx_CV.Checked ? true : false;

            // Use Multi-Thread or not
            if (checkBx_MultiThread.Checked)
            {
                IO.IsMultiThread = true;
                IO.Thread = IO.Cpu_cores;
            }
            else
            {
                IO.IsMultiThread = false;
                IO.Thread = 1;
            }

            this.Close();
        }

        private void SimulatorSetting_Load(object sender, EventArgs e)
        {
            txtNSteps.Text = "100";
            txtNSims.Text = "10000";
            checkBx_Anti.Checked = false;
            checkBx_CV.Checked = false;
            checkBx_MultiThread.Checked = true;

            IO.NSteps = Convert.ToInt32(txtNSteps.Text);
            IO.NSims = Convert.ToInt64(txtNSims.Text);

            // Use Antithetic or not
            IO.IsAnti = checkBx_Anti.Checked ? true : false;

            // Use Control Variate or not
            IO.IsCv = checkBx_CV.Checked ? true : false;

            // Use Multi-Thread or not
            if (checkBx_MultiThread.Checked)
            {
                IO.IsMultiThread = true;
                IO.Thread = IO.Cpu_cores;
            }
            else
            {
                IO.IsMultiThread = false;
                IO.Thread = 1;
            }
        }
    }
}
