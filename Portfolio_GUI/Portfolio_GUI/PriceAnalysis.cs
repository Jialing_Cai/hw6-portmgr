﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class PriceAnalysis : Form
    {
        public PriceAnalysis()
        {
            InitializeComponent();
        }

        DataModelContainer db = new DataModelContainer();

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PriceAnalysis_Load(object sender, EventArgs e)
        {
            var p = (from g in db.HistoryPrices
                     join gg in db.Instruments
                     on g.InstrumentId equals gg.Id
                     select new { gg.Ticker, g.Date, g.ClosingPrice }).ToList();

            gridPriceAnalysis.DataSource = p;

            foreach (var g in p) { if (!cmbInstrument.Items.Contains(g.Ticker)) { cmbInstrument.Items.Add(g.Ticker); } }
        }

        private void cmbInstrument_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var p = (from g in db.HistoryPrices
                         join gg in db.Instruments
                         on g.InstrumentId equals gg.Id
                         where gg.Ticker == cmbInstrument.Text
                         select new { gg.Ticker, g.Date, g.ClosingPrice }).OrderByDescending(x => x.Date).ToList();
                gridPriceAnalysis.DataSource = p;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
