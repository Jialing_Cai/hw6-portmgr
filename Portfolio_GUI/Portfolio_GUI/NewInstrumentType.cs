﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class NewInstrumentType : Form
    {
        public NewInstrumentType()
        {
            InitializeComponent();
        }

        DataModelContainer db = new DataModelContainer();

        private void NewInstrumentType_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'instTypeDataSet.InstTypes' table. You can move, or remove it, as needed.
            this.instTypesTableAdapter.Fill(this.instTypeDataSet.InstTypes);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var g = (from g0 in db.InstTypes
                         select g0.TypeName).ToList();

                if (radioAsian.Checked)
                {
                    if (g.Contains("Asian"))
                    {
                        MessageBox.Show("Already exists!");
                    }
                    else
                    {
                        db.InstTypes.Add(new InstType
                        {
                            TypeName = "Asian"
                        });
                    }
                }
                else if (radioBarrier.Checked)
                {
                    if (g.Contains("Barrier"))
                    {
                        MessageBox.Show("Already exists!");
                    }
                    else
                    {
                        db.InstTypes.Add(new InstType
                        {
                            TypeName = "Barrier"
                        });
                    }
                }
                else if (radioDigital.Checked)
                {
                    if (g.Contains("Digital"))
                    {
                        MessageBox.Show("Already exists!");
                    }
                    else
                    {
                        db.InstTypes.Add(new InstType
                        {
                            TypeName = "Digital"
                        });
                    }
                }
                else if (radioLookback.Checked)
                {
                    if (g.Contains("Lookback"))
                    {
                        MessageBox.Show("Already exists!");
                    }
                    else
                    {
                        db.InstTypes.Add(new InstType
                        {
                            TypeName = "Lookback"
                        });
                    }
                }
                else if (radioRange.Checked)
                {
                    if (g.Contains("Range"))
                    {
                        MessageBox.Show("Already exists!");
                    }
                    else
                    {
                        db.InstTypes.Add(new InstType
                        {
                            TypeName = "Range"
                        });
                    }
                }
                else if (radioEuro.Checked)
                {
                    if (g.Contains("European"))
                    {
                        MessageBox.Show("Already exists!");
                    }
                    else
                    {
                        db.InstTypes.Add(new InstType
                        {
                            TypeName = "European"
                        });
                    }
                }
                else // stock
                {
                    if (g.Contains("Stock"))
                    {
                        MessageBox.Show("Already exists!");
                    }
                    else
                    {
                        db.InstTypes.Add(new InstType
                        {
                            TypeName = "Stock"
                        });
                    }
                }

                db.SaveChanges();
                NewInstrumentType_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {            
            NewInstrumentType_Load(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in gridInstType.SelectedRows)
                {
                    gridInstType.Rows.Remove(row);
                }

                this.instTypesTableAdapter.Update(this.instTypeDataSet.InstTypes);
                NewInstrumentType_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
    }
}
