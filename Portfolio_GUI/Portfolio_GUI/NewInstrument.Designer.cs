﻿namespace Portfolio_GUI
{
    partial class NewInstrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.txtTicker = new System.Windows.Forms.TextBox();
            this.txtExchange = new System.Windows.Forms.TextBox();
            this.txtStrike = new System.Windows.Forms.TextBox();
            this.txtTenor = new System.Windows.Forms.TextBox();
            this.cmbUnderlying = new System.Windows.Forms.ComboBox();
            this.cmbInstrType = new System.Windows.Forms.ComboBox();
            this.cmbCallPut = new System.Windows.Forms.ComboBox();
            this.cmbBarrierType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRebate = new System.Windows.Forms.TextBox();
            this.txtBarrier = new System.Windows.Forms.TextBox();
            this.btnSaveStock = new System.Windows.Forms.Button();
            this.btnSaveOption = new System.Windows.Forms.Button();
            this.grpOption = new System.Windows.Forms.GroupBox();
            this.grpBarrier = new System.Windows.Forms.GroupBox();
            this.grpDigital = new System.Windows.Forms.GroupBox();
            this.grpStock = new System.Windows.Forms.GroupBox();
            this.portfolioManagerDataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.instrumentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridInstrument = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tickerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exchangeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.optionTypeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isCallDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tenorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strikeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rebateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barrierDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barrierTypeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instTypeIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instrumentsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.instrumentsDataSet = new Portfolio_GUI.InstrumentsDataSet();
            this.instrumentsTableAdapter = new Portfolio_GUI.InstrumentsDataSetTableAdapters.InstrumentsTableAdapter();
            this.grpOption.SuspendLayout();
            this.grpBarrier.SuspendLayout();
            this.grpDigital.SuspendLayout();
            this.grpStock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portfolioManagerDataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridInstrument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentsDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(97, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Inst. Type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ticker:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(89, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Type:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tenor:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Strike:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(53, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Underlying:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(59, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Exchange:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(139, 44);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(131, 22);
            this.txtCompanyName.TabIndex = 8;
            // 
            // txtTicker
            // 
            this.txtTicker.Location = new System.Drawing.Point(139, 72);
            this.txtTicker.Name = "txtTicker";
            this.txtTicker.Size = new System.Drawing.Size(131, 22);
            this.txtTicker.TabIndex = 9;
            // 
            // txtExchange
            // 
            this.txtExchange.Location = new System.Drawing.Point(139, 100);
            this.txtExchange.Name = "txtExchange";
            this.txtExchange.Size = new System.Drawing.Size(131, 22);
            this.txtExchange.TabIndex = 10;
            // 
            // txtStrike
            // 
            this.txtStrike.Location = new System.Drawing.Point(139, 101);
            this.txtStrike.Name = "txtStrike";
            this.txtStrike.Size = new System.Drawing.Size(131, 22);
            this.txtStrike.TabIndex = 12;
            // 
            // txtTenor
            // 
            this.txtTenor.Location = new System.Drawing.Point(139, 129);
            this.txtTenor.Name = "txtTenor";
            this.txtTenor.Size = new System.Drawing.Size(131, 22);
            this.txtTenor.TabIndex = 13;
            // 
            // cmbUnderlying
            // 
            this.cmbUnderlying.FormattingEnabled = true;
            this.cmbUnderlying.Location = new System.Drawing.Point(139, 41);
            this.cmbUnderlying.Name = "cmbUnderlying";
            this.cmbUnderlying.Size = new System.Drawing.Size(131, 24);
            this.cmbUnderlying.TabIndex = 17;
            // 
            // cmbInstrType
            // 
            this.cmbInstrType.FormattingEnabled = true;
            this.cmbInstrType.Location = new System.Drawing.Point(177, 27);
            this.cmbInstrType.Name = "cmbInstrType";
            this.cmbInstrType.Size = new System.Drawing.Size(131, 24);
            this.cmbInstrType.TabIndex = 18;
            this.cmbInstrType.SelectedIndexChanged += new System.EventHandler(this.cmbInstrType_SelectedIndexChanged);
            // 
            // cmbCallPut
            // 
            this.cmbCallPut.FormattingEnabled = true;
            this.cmbCallPut.Items.AddRange(new object[] {
            "Call",
            "Put"});
            this.cmbCallPut.Location = new System.Drawing.Point(139, 71);
            this.cmbCallPut.Name = "cmbCallPut";
            this.cmbCallPut.Size = new System.Drawing.Size(131, 24);
            this.cmbCallPut.TabIndex = 19;
            // 
            // cmbBarrierType
            // 
            this.cmbBarrierType.FormattingEnabled = true;
            this.cmbBarrierType.Items.AddRange(new object[] {
            "None",
            "Up and In",
            "Up and Out",
            "Down and In",
            "Down and Out"});
            this.cmbBarrierType.Location = new System.Drawing.Point(139, 27);
            this.cmbBarrierType.Name = "cmbBarrierType";
            this.cmbBarrierType.Size = new System.Drawing.Size(131, 24);
            this.cmbBarrierType.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(42, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Barrier Type:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(78, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Barrier:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(78, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 17);
            this.label11.TabIndex = 23;
            this.label11.Text = "Rebate:";
            // 
            // txtRebate
            // 
            this.txtRebate.Location = new System.Drawing.Point(142, 32);
            this.txtRebate.Name = "txtRebate";
            this.txtRebate.Size = new System.Drawing.Size(131, 22);
            this.txtRebate.TabIndex = 24;
            // 
            // txtBarrier
            // 
            this.txtBarrier.Location = new System.Drawing.Point(139, 57);
            this.txtBarrier.Name = "txtBarrier";
            this.txtBarrier.Size = new System.Drawing.Size(131, 22);
            this.txtBarrier.TabIndex = 25;
            // 
            // btnSaveStock
            // 
            this.btnSaveStock.Location = new System.Drawing.Point(38, 208);
            this.btnSaveStock.Name = "btnSaveStock";
            this.btnSaveStock.Size = new System.Drawing.Size(301, 37);
            this.btnSaveStock.TabIndex = 26;
            this.btnSaveStock.Text = "Save to DB as Stock";
            this.btnSaveStock.UseVisualStyleBackColor = true;
            this.btnSaveStock.Click += new System.EventHandler(this.btnSaveStock_Click);
            // 
            // btnSaveOption
            // 
            this.btnSaveOption.Location = new System.Drawing.Point(360, 208);
            this.btnSaveOption.Name = "btnSaveOption";
            this.btnSaveOption.Size = new System.Drawing.Size(617, 37);
            this.btnSaveOption.TabIndex = 27;
            this.btnSaveOption.Text = "Save to DB as Option";
            this.btnSaveOption.UseVisualStyleBackColor = true;
            this.btnSaveOption.Click += new System.EventHandler(this.btnSaveOption_Click);
            // 
            // grpOption
            // 
            this.grpOption.Controls.Add(this.label4);
            this.grpOption.Controls.Add(this.label5);
            this.grpOption.Controls.Add(this.label6);
            this.grpOption.Controls.Add(this.label7);
            this.grpOption.Controls.Add(this.txtStrike);
            this.grpOption.Controls.Add(this.txtTenor);
            this.grpOption.Controls.Add(this.cmbUnderlying);
            this.grpOption.Controls.Add(this.cmbCallPut);
            this.grpOption.Location = new System.Drawing.Point(360, 30);
            this.grpOption.Name = "grpOption";
            this.grpOption.Size = new System.Drawing.Size(301, 169);
            this.grpOption.TabIndex = 28;
            this.grpOption.TabStop = false;
            this.grpOption.Text = "Option";
            // 
            // grpBarrier
            // 
            this.grpBarrier.Controls.Add(this.txtBarrier);
            this.grpBarrier.Controls.Add(this.cmbBarrierType);
            this.grpBarrier.Controls.Add(this.label9);
            this.grpBarrier.Controls.Add(this.label10);
            this.grpBarrier.Location = new System.Drawing.Point(676, 30);
            this.grpBarrier.Name = "grpBarrier";
            this.grpBarrier.Size = new System.Drawing.Size(301, 99);
            this.grpBarrier.TabIndex = 29;
            this.grpBarrier.TabStop = false;
            this.grpBarrier.Text = "Barrier Option";
            // 
            // grpDigital
            // 
            this.grpDigital.Controls.Add(this.txtRebate);
            this.grpDigital.Controls.Add(this.label11);
            this.grpDigital.Location = new System.Drawing.Point(676, 135);
            this.grpDigital.Name = "grpDigital";
            this.grpDigital.Size = new System.Drawing.Size(301, 64);
            this.grpDigital.TabIndex = 30;
            this.grpDigital.TabStop = false;
            this.grpDigital.Text = "Digital Option";
            // 
            // grpStock
            // 
            this.grpStock.Controls.Add(this.label3);
            this.grpStock.Controls.Add(this.label1);
            this.grpStock.Controls.Add(this.label8);
            this.grpStock.Controls.Add(this.txtCompanyName);
            this.grpStock.Controls.Add(this.txtTicker);
            this.grpStock.Controls.Add(this.txtExchange);
            this.grpStock.Location = new System.Drawing.Point(38, 60);
            this.grpStock.Name = "grpStock";
            this.grpStock.Size = new System.Drawing.Size(301, 139);
            this.grpStock.TabIndex = 31;
            this.grpStock.TabStop = false;
            this.grpStock.Text = "Stock";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(38, 267);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(77, 26);
            this.btnRefresh.TabIndex = 33;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(121, 267);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(167, 26);
            this.btnDelete.TabIndex = 34;
            this.btnDelete.Text = "Delete and Save";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // gridInstrument
            // 
            this.gridInstrument.AllowUserToAddRows = false;
            this.gridInstrument.AllowUserToDeleteRows = false;
            this.gridInstrument.AutoGenerateColumns = false;
            this.gridInstrument.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridInstrument.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.tickerDataGridViewTextBoxColumn,
            this.companyNameDataGridViewTextBoxColumn,
            this.exchangeDataGridViewTextBoxColumn,
            this.optionTypeNameDataGridViewTextBoxColumn,
            this.isCallDataGridViewCheckBoxColumn,
            this.tenorDataGridViewTextBoxColumn,
            this.strikeDataGridViewTextBoxColumn,
            this.rebateDataGridViewTextBoxColumn,
            this.barrierDataGridViewTextBoxColumn,
            this.barrierTypeNameDataGridViewTextBoxColumn,
            this.instTypeIdDataGridViewTextBoxColumn});
            this.gridInstrument.DataSource = this.instrumentsBindingSource1;
            this.gridInstrument.Location = new System.Drawing.Point(38, 299);
            this.gridInstrument.Name = "gridInstrument";
            this.gridInstrument.RowTemplate.Height = 24;
            this.gridInstrument.Size = new System.Drawing.Size(939, 231);
            this.gridInstrument.TabIndex = 35;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tickerDataGridViewTextBoxColumn
            // 
            this.tickerDataGridViewTextBoxColumn.DataPropertyName = "Ticker";
            this.tickerDataGridViewTextBoxColumn.HeaderText = "Ticker";
            this.tickerDataGridViewTextBoxColumn.Name = "tickerDataGridViewTextBoxColumn";
            // 
            // companyNameDataGridViewTextBoxColumn
            // 
            this.companyNameDataGridViewTextBoxColumn.DataPropertyName = "CompanyName";
            this.companyNameDataGridViewTextBoxColumn.HeaderText = "CompanyName";
            this.companyNameDataGridViewTextBoxColumn.Name = "companyNameDataGridViewTextBoxColumn";
            // 
            // exchangeDataGridViewTextBoxColumn
            // 
            this.exchangeDataGridViewTextBoxColumn.DataPropertyName = "Exchange";
            this.exchangeDataGridViewTextBoxColumn.HeaderText = "Exchange";
            this.exchangeDataGridViewTextBoxColumn.Name = "exchangeDataGridViewTextBoxColumn";
            // 
            // optionTypeNameDataGridViewTextBoxColumn
            // 
            this.optionTypeNameDataGridViewTextBoxColumn.DataPropertyName = "OptionTypeName";
            this.optionTypeNameDataGridViewTextBoxColumn.HeaderText = "OptionTypeName";
            this.optionTypeNameDataGridViewTextBoxColumn.Name = "optionTypeNameDataGridViewTextBoxColumn";
            // 
            // isCallDataGridViewCheckBoxColumn
            // 
            this.isCallDataGridViewCheckBoxColumn.DataPropertyName = "IsCall";
            this.isCallDataGridViewCheckBoxColumn.HeaderText = "IsCall";
            this.isCallDataGridViewCheckBoxColumn.Name = "isCallDataGridViewCheckBoxColumn";
            // 
            // tenorDataGridViewTextBoxColumn
            // 
            this.tenorDataGridViewTextBoxColumn.DataPropertyName = "Tenor";
            this.tenorDataGridViewTextBoxColumn.HeaderText = "Tenor";
            this.tenorDataGridViewTextBoxColumn.Name = "tenorDataGridViewTextBoxColumn";
            // 
            // strikeDataGridViewTextBoxColumn
            // 
            this.strikeDataGridViewTextBoxColumn.DataPropertyName = "Strike";
            this.strikeDataGridViewTextBoxColumn.HeaderText = "Strike";
            this.strikeDataGridViewTextBoxColumn.Name = "strikeDataGridViewTextBoxColumn";
            // 
            // rebateDataGridViewTextBoxColumn
            // 
            this.rebateDataGridViewTextBoxColumn.DataPropertyName = "Rebate";
            this.rebateDataGridViewTextBoxColumn.HeaderText = "Rebate";
            this.rebateDataGridViewTextBoxColumn.Name = "rebateDataGridViewTextBoxColumn";
            // 
            // barrierDataGridViewTextBoxColumn
            // 
            this.barrierDataGridViewTextBoxColumn.DataPropertyName = "Barrier";
            this.barrierDataGridViewTextBoxColumn.HeaderText = "Barrier";
            this.barrierDataGridViewTextBoxColumn.Name = "barrierDataGridViewTextBoxColumn";
            // 
            // barrierTypeNameDataGridViewTextBoxColumn
            // 
            this.barrierTypeNameDataGridViewTextBoxColumn.DataPropertyName = "BarrierTypeName";
            this.barrierTypeNameDataGridViewTextBoxColumn.HeaderText = "BarrierTypeName";
            this.barrierTypeNameDataGridViewTextBoxColumn.Name = "barrierTypeNameDataGridViewTextBoxColumn";
            // 
            // instTypeIdDataGridViewTextBoxColumn
            // 
            this.instTypeIdDataGridViewTextBoxColumn.DataPropertyName = "InstTypeId";
            this.instTypeIdDataGridViewTextBoxColumn.HeaderText = "InstTypeId";
            this.instTypeIdDataGridViewTextBoxColumn.Name = "instTypeIdDataGridViewTextBoxColumn";
            // 
            // instrumentsBindingSource1
            // 
            this.instrumentsBindingSource1.DataMember = "Instruments";
            this.instrumentsBindingSource1.DataSource = this.instrumentsDataSet;
            // 
            // instrumentsDataSet
            // 
            this.instrumentsDataSet.DataSetName = "InstrumentsDataSet";
            this.instrumentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // instrumentsTableAdapter
            // 
            this.instrumentsTableAdapter.ClearBeforeFill = true;
            // 
            // NewInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 572);
            this.Controls.Add(this.gridInstrument);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.grpStock);
            this.Controls.Add(this.grpDigital);
            this.Controls.Add(this.grpBarrier);
            this.Controls.Add(this.grpOption);
            this.Controls.Add(this.btnSaveOption);
            this.Controls.Add(this.btnSaveStock);
            this.Controls.Add(this.cmbInstrType);
            this.Controls.Add(this.label2);
            this.Name = "NewInstrument";
            this.Text = "New Instrument...";
            this.Load += new System.EventHandler(this.NewInstrument_Load);
            this.grpOption.ResumeLayout(false);
            this.grpOption.PerformLayout();
            this.grpBarrier.ResumeLayout(false);
            this.grpBarrier.PerformLayout();
            this.grpDigital.ResumeLayout(false);
            this.grpDigital.PerformLayout();
            this.grpStock.ResumeLayout(false);
            this.grpStock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portfolioManagerDataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridInstrument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instrumentsDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.TextBox txtTicker;
        private System.Windows.Forms.TextBox txtExchange;
        private System.Windows.Forms.TextBox txtStrike;
        private System.Windows.Forms.TextBox txtTenor;
        private System.Windows.Forms.ComboBox cmbUnderlying;
        private System.Windows.Forms.ComboBox cmbInstrType;
        private System.Windows.Forms.ComboBox cmbCallPut;
        private System.Windows.Forms.ComboBox cmbBarrierType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtRebate;
        private System.Windows.Forms.Button btnSaveStock;
        private System.Windows.Forms.Button btnSaveOption;
        public System.Windows.Forms.TextBox txtBarrier;
        private System.Windows.Forms.GroupBox grpOption;
        private System.Windows.Forms.GroupBox grpBarrier;
        private System.Windows.Forms.GroupBox grpDigital;
        private System.Windows.Forms.GroupBox grpStock;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.BindingSource instrumentsBindingSource;
        private System.Windows.Forms.BindingSource portfolioManagerDataSet1BindingSource;
        private System.Windows.Forms.DataGridView gridInstrument;
        private InstrumentsDataSet instrumentsDataSet;
        private System.Windows.Forms.BindingSource instrumentsBindingSource1;
        private InstrumentsDataSetTableAdapters.InstrumentsTableAdapter instrumentsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tickerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn exchangeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn optionTypeNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isCallDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn strikeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rebateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn barrierDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn barrierTypeNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn instTypeIdDataGridViewTextBoxColumn;
    }
}