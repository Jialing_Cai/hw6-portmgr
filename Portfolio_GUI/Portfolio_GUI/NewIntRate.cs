﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class NewIntRate : Form
    {
        public NewIntRate()
        {
            InitializeComponent();
        }

        DataModelContainer db = new DataModelContainer();

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((from g in db.InterestRates
                     select g.Tenor).ToList().Contains(Convert.ToDouble(txtTenor.Text)))
                {
                    MessageBox.Show("Already exists the same tenor!");
                }
                else
                {
                    db.InterestRates.Add(new InterestRate
                    {
                        Tenor = Convert.ToDouble(txtTenor.Text),
                        Rate = Convert.ToDouble(txtIRate.Text)
                    });

                    db.SaveChanges();
                    this.interestRatesTableAdapter1.Fill(this.iRateDataSet.InterestRates);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void NewIntRate_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'iRateDataSet.InterestRates' table. You can move, or remove it, as needed.
            this.interestRatesTableAdapter1.Fill(this.iRateDataSet.InterestRates);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.interestRatesTableAdapter1.Fill(this.iRateDataSet.InterestRates);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in gridRate.SelectedRows)
                {
                    gridRate.Rows.Remove(row);
                }

                this.interestRatesTableAdapter1.Update(this.iRateDataSet.InterestRates);
                this.interestRatesTableAdapter1.Fill(this.iRateDataSet.InterestRates);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
