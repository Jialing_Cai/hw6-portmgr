﻿namespace Portfolio_GUI
{
    partial class SimulatorSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.grpBoxAnti = new System.Windows.Forms.GroupBox();
            this.checkBx_MultiThread = new System.Windows.Forms.CheckBox();
            this.checkBx_CV = new System.Windows.Forms.CheckBox();
            this.checkBx_Anti = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtNSims = new System.Windows.Forms.TextBox();
            this.txtNSteps = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grpBoxAnti.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 17);
            this.label10.TabIndex = 39;
            this.label10.Text = "Variance Reduction:";
            // 
            // grpBoxAnti
            // 
            this.grpBoxAnti.Controls.Add(this.checkBx_MultiThread);
            this.grpBoxAnti.Controls.Add(this.checkBx_CV);
            this.grpBoxAnti.Controls.Add(this.checkBx_Anti);
            this.grpBoxAnti.Location = new System.Drawing.Point(32, 107);
            this.grpBoxAnti.Name = "grpBoxAnti";
            this.grpBoxAnti.Size = new System.Drawing.Size(226, 130);
            this.grpBoxAnti.TabIndex = 38;
            this.grpBoxAnti.TabStop = false;
            // 
            // checkBx_MultiThread
            // 
            this.checkBx_MultiThread.AllowDrop = true;
            this.checkBx_MultiThread.AutoSize = true;
            this.checkBx_MultiThread.Checked = true;
            this.checkBx_MultiThread.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBx_MultiThread.Location = new System.Drawing.Point(6, 94);
            this.checkBx_MultiThread.Name = "checkBx_MultiThread";
            this.checkBx_MultiThread.Size = new System.Drawing.Size(129, 21);
            this.checkBx_MultiThread.TabIndex = 2;
            this.checkBx_MultiThread.Text = "Multi-Threading";
            this.checkBx_MultiThread.UseVisualStyleBackColor = true;
            // 
            // checkBx_CV
            // 
            this.checkBx_CV.AutoSize = true;
            this.checkBx_CV.Location = new System.Drawing.Point(6, 62);
            this.checkBx_CV.Name = "checkBx_CV";
            this.checkBx_CV.Size = new System.Drawing.Size(206, 21);
            this.checkBx_CV.TabIndex = 1;
            this.checkBx_CV.Text = "Delta-Based Control Variate";
            this.checkBx_CV.UseVisualStyleBackColor = true;
            // 
            // checkBx_Anti
            // 
            this.checkBx_Anti.AutoSize = true;
            this.checkBx_Anti.Location = new System.Drawing.Point(6, 31);
            this.checkBx_Anti.Name = "checkBx_Anti";
            this.checkBx_Anti.Size = new System.Drawing.Size(82, 21);
            this.checkBx_Anti.TabIndex = 0;
            this.checkBx_Anti.Text = "Antithetic";
            this.checkBx_Anti.UseCompatibleTextRendering = true;
            this.checkBx_Anti.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(32, 263);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(226, 30);
            this.btnSave.TabIndex = 40;
            this.btnSave.Text = "Save and Exit";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtNSims
            // 
            this.txtNSims.Location = new System.Drawing.Point(82, 51);
            this.txtNSims.Name = "txtNSims";
            this.txtNSims.Size = new System.Drawing.Size(125, 22);
            this.txtNSims.TabIndex = 44;
            // 
            // txtNSteps
            // 
            this.txtNSteps.Location = new System.Drawing.Point(82, 23);
            this.txtNSteps.Name = "txtNSteps";
            this.txtNSteps.Size = new System.Drawing.Size(125, 22);
            this.txtNSteps.TabIndex = 43;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 17);
            this.label8.TabIndex = 42;
            this.label8.Text = "Steps:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 41;
            this.label2.Text = "Trials:";
            // 
            // SimulatorSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 310);
            this.Controls.Add(this.txtNSims);
            this.Controls.Add(this.txtNSteps);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.grpBoxAnti);
            this.Name = "SimulatorSetting";
            this.Text = "Simulator Setting";
            this.Load += new System.EventHandler(this.SimulatorSetting_Load);
            this.grpBoxAnti.ResumeLayout(false);
            this.grpBoxAnti.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox grpBoxAnti;
        private System.Windows.Forms.CheckBox checkBx_MultiThread;
        private System.Windows.Forms.CheckBox checkBx_CV;
        private System.Windows.Forms.CheckBox checkBx_Anti;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtNSims;
        private System.Windows.Forms.TextBox txtNSteps;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
    }
}