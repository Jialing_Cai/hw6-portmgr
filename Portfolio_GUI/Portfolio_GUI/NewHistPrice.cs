﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Portfolio_Manager;

namespace Portfolio_GUI
{
    public partial class NewHistPrice : Form
    {
        public NewHistPrice()
        {
            InitializeComponent();
        }

        DataModelContainer db = new DataModelContainer();

        private void NewHistPrice_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'histPriceDataSet.HistoryPrices' table. You can move, or remove it, as needed.
            this.historyPricesTableAdapter.Fill(this.histPriceDataSet.HistoryPrices);

            dateTimePicker1.MaxDate = DateTime.Today;

            var g = (from g1 in db.Instruments
                     join g2 in db.InstTypes
                     on g1.InstTypeId equals g2.Id
                     where g2.TypeName == "Stock"
                     select g1.Ticker).ToList();
            foreach (var gg in g) { cmbStock.Items.Add(gg); }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db.HistoryPrices.Add(new HistoryPrice
                {
                    Date = dateTimePicker1.Value,
                    ClosingPrice = Convert.ToDouble(txtClosingPx.Text),
                    InstrumentId = (from g in db.Instruments
                                    join gg in db.InstTypes
                                    on g.InstTypeId equals gg.Id
                                    where gg.TypeName == "Stock" && g.Ticker == cmbStock.Text
                                    select g.Id).FirstOrDefault()

                });
                db.SaveChanges();
                this.historyPricesTableAdapter.Fill(this.histPriceDataSet.HistoryPrices);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }        

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.historyPricesTableAdapter.Fill(this.histPriceDataSet.HistoryPrices);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in gridPrice.SelectedRows)
                {
                    gridPrice.Rows.Remove(row);
                }

                this.historyPricesTableAdapter.Update(this.histPriceDataSet.HistoryPrices);
                this.historyPricesTableAdapter.Fill(this.histPriceDataSet.HistoryPrices);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
