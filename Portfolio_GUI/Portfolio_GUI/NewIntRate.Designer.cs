﻿namespace Portfolio_GUI
{
    partial class NewIntRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenor = new System.Windows.Forms.TextBox();
            this.txtIRate = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.gridRate = new System.Windows.Forms.DataGridView();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();            
            this.tenorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRateDataSet = new Portfolio_GUI.IRateDataSet();
            this.interestRatesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.interestRatesTableAdapter1 = new Portfolio_GUI.IRateDataSetTableAdapters.InterestRatesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.gridRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iRateDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestRatesBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tenor:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Interest Rate:";
            // 
            // txtTenor
            // 
            this.txtTenor.Location = new System.Drawing.Point(138, 77);
            this.txtTenor.Name = "txtTenor";
            this.txtTenor.Size = new System.Drawing.Size(105, 22);
            this.txtTenor.TabIndex = 2;
            // 
            // txtIRate
            // 
            this.txtIRate.Location = new System.Drawing.Point(138, 112);
            this.txtIRate.Name = "txtIRate";
            this.txtIRate.Size = new System.Drawing.Size(105, 22);
            this.txtIRate.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(36, 160);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(209, 33);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save to DB";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gridRate
            // 
            this.gridRate.AllowUserToAddRows = false;
            this.gridRate.AllowUserToDeleteRows = false;
            this.gridRate.AutoGenerateColumns = false;
            this.gridRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tenorDataGridViewTextBoxColumn,
            this.rateDataGridViewTextBoxColumn});
            this.gridRate.DataSource = this.interestRatesBindingSource1;
            this.gridRate.Location = new System.Drawing.Point(264, 60);
            this.gridRate.Name = "gridRate";
            this.gridRate.RowTemplate.Height = 24;
            this.gridRate.Size = new System.Drawing.Size(298, 133);
            this.gridRate.TabIndex = 5;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(264, 29);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(88, 25);
            this.btnRefresh.TabIndex = 13;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(394, 29);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(167, 25);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "Delete and Save";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);            
            // 
            // tenorDataGridViewTextBoxColumn
            // 
            this.tenorDataGridViewTextBoxColumn.DataPropertyName = "Tenor";
            this.tenorDataGridViewTextBoxColumn.HeaderText = "Tenor";
            this.tenorDataGridViewTextBoxColumn.Name = "tenorDataGridViewTextBoxColumn";
            // 
            // rateDataGridViewTextBoxColumn
            // 
            this.rateDataGridViewTextBoxColumn.DataPropertyName = "Rate";
            this.rateDataGridViewTextBoxColumn.HeaderText = "Rate";
            this.rateDataGridViewTextBoxColumn.Name = "rateDataGridViewTextBoxColumn";
            // 
            // iRateDataSet
            // 
            this.iRateDataSet.DataSetName = "IRateDataSet";
            this.iRateDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // interestRatesBindingSource1
            // 
            this.interestRatesBindingSource1.DataMember = "InterestRates";
            this.interestRatesBindingSource1.DataSource = this.iRateDataSet;
            // 
            // interestRatesTableAdapter1
            // 
            this.interestRatesTableAdapter1.ClearBeforeFill = true;
            // 
            // NewIntRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 223);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.gridRate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtIRate);
            this.Controls.Add(this.txtTenor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewIntRate";
            this.Text = "New Interest Rate...";
            this.Load += new System.EventHandler(this.NewIntRate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iRateDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestRatesBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTenor;
        private System.Windows.Forms.TextBox txtIRate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView gridRate;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateDataGridViewTextBoxColumn;
        private IRateDataSet iRateDataSet;
        private System.Windows.Forms.BindingSource interestRatesBindingSource1;
        private IRateDataSetTableAdapters.InterestRatesTableAdapter interestRatesTableAdapter1;
    }
}