﻿namespace Portfolio_GUI
{
    partial class PriceAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbInstrument = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.gridPriceAnalysis = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridPriceAnalysis)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Instrument:";
            // 
            // cmbInstrument
            // 
            this.cmbInstrument.FormattingEnabled = true;
            this.cmbInstrument.Location = new System.Drawing.Point(120, 29);
            this.cmbInstrument.Name = "cmbInstrument";
            this.cmbInstrument.Size = new System.Drawing.Size(153, 24);
            this.cmbInstrument.TabIndex = 1;
            this.cmbInstrument.SelectedIndexChanged += new System.EventHandler(this.cmbInstrument_SelectedIndexChanged);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(432, 294);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(92, 24);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gridPriceAnalysis
            // 
            this.gridPriceAnalysis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPriceAnalysis.Location = new System.Drawing.Point(33, 59);
            this.gridPriceAnalysis.Name = "gridPriceAnalysis";
            this.gridPriceAnalysis.RowTemplate.Height = 24;
            this.gridPriceAnalysis.Size = new System.Drawing.Size(491, 229);
            this.gridPriceAnalysis.TabIndex = 4;
            // 
            // PriceAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 331);
            this.Controls.Add(this.gridPriceAnalysis);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.cmbInstrument);
            this.Controls.Add(this.label1);
            this.Name = "PriceAnalysis";
            this.Text = "Price Analysis";
            this.Load += new System.EventHandler(this.PriceAnalysis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridPriceAnalysis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbInstrument;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView gridPriceAnalysis;
    }
}