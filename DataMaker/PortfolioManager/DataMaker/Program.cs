﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portfolio_Manager;

namespace DataMaker
{
    public class Program
    {

        static public void Main(string[] args)
        {
            try
            {
                DataModelContainer db = new DataModelContainer();

                db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.HistoryPrices");
                Console.WriteLine("Clearing table HistoryPrices Succeeds!");

                db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.InterestRates");
                Console.WriteLine("Clearing table InterestRates Succeeds!");

                db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Trades");
                Console.WriteLine("Clearing table Trades Succeeds!");

                foreach (var g in db.Instruments) { db.Instruments.Remove(g); }
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('dbo.Instruments', RESEED, 0)");
                Console.WriteLine("Clearing table Instruments Succeeds!");

                foreach (var g in db.InstTypes) { db.InstTypes.Remove(g); }
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('dbo.InstTypes', RESEED, 0)");
                Console.WriteLine("Clearing table InstTypes Succeeds!");

                db.SaveChanges();
                Console.WriteLine();

                db.InstTypes.Add(new InstType { TypeName = "Stock" });
                db.InstTypes.Add(new InstType { TypeName = "European Option" });
                db.InstTypes.Add(new InstType { TypeName = "Asian Option" });
                db.InstTypes.Add(new InstType { TypeName = "Digital Option" });
                db.InstTypes.Add(new InstType { TypeName = "Barrier Option" });
                db.SaveChanges();
                Console.WriteLine("Adding InstTypes Succeeds!");

                db.Instruments.Add(new Instrument
                {
                    Ticker = "MSFT",
                    CompanyName = "Microsoft",
                    Exchange = "NASDAQ",
                    OptionTypeName = "Stock on MSFT",
                    IsCall = true,
                    Tenor = 0,
                    Strike = 0,
                    Barrier = 0,
                    Rebate = 0,
                    BarrierTypeName = "None",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == "Stock"
                                  select insttypes.Id).FirstOrDefault()
                });
                db.Instruments.Add(new Instrument
                {
                    Ticker = "AMZN",
                    CompanyName = "Amazon",
                    Exchange = "NASDAQ",
                    OptionTypeName = "Stock on AMZN",
                    IsCall = true,
                    Tenor = 0,
                    Strike = 0,
                    Barrier = 0,
                    Rebate = 0,
                    BarrierTypeName = "None",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == "Stock"
                                  select insttypes.Id).FirstOrDefault()
                });
                db.Instruments.Add(new Instrument
                {
                    Ticker = "APPL",
                    CompanyName = "Apple",
                    Exchange = "NASDAQ",
                    OptionTypeName = "Stock on APPL",
                    IsCall = true,
                    Tenor = 0,
                    Strike = 0,
                    Barrier = 0,
                    Rebate = 0,
                    BarrierTypeName = "None",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == "Stock"
                                  select insttypes.Id).FirstOrDefault()
                });
                db.Instruments.Add(new Instrument
                {
                    Ticker = "APPL",
                    CompanyName = "Apple",
                    Exchange = "NASDAQ",
                    OptionTypeName = "Put Digital Option on APPL",
                    IsCall = false,
                    Tenor = 5,
                    Strike = 0,
                    Barrier = 0,
                    Rebate = 3,
                    BarrierTypeName = "None",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == "Digital Option"
                                  select insttypes.Id).FirstOrDefault()
                });
                db.Instruments.Add(new Instrument
                {
                    Ticker = "MSFT",
                    CompanyName = "Microsoft",
                    Exchange = "NASDAQ",
                    OptionTypeName = "Call European Option on MSFT",
                    IsCall = true,
                    Tenor = 0.5,
                    Strike = 50,
                    Barrier = 0,
                    Rebate = 0,
                    BarrierTypeName = "None",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == "European Option"
                                  select insttypes.Id).FirstOrDefault()
                });
                db.Instruments.Add(new Instrument
                {
                    Ticker = "MSFT",
                    CompanyName = "Microsoft",
                    Exchange = "NASDAQ",
                    OptionTypeName = "Put European Option on MSFT",
                    IsCall = false,
                    Tenor = 0.5,
                    Strike = 50,
                    Barrier = 0,
                    Rebate = 0,
                    BarrierTypeName = "None",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == "European Option"
                                  select insttypes.Id).FirstOrDefault()
                });
                db.Instruments.Add(new Instrument
                {
                    Ticker = "AMZN",
                    CompanyName = "Amazon",
                    Exchange = "NASDAQ",
                    OptionTypeName = "Put Barrier Option on AMZN",
                    IsCall = false,
                    Tenor = 1.5,
                    Strike = 0,
                    Barrier = 5,
                    Rebate = 0,
                    BarrierTypeName = "Up and In",
                    InstTypeId = (from insttypes in db.InstTypes
                                  where insttypes.TypeName == "Barrier Option"
                                  select insttypes.Id).FirstOrDefault()
                });
                db.SaveChanges();
                Console.WriteLine("Adding Insruments Succeeds!");

                db.Trades.Add(new Trade
                {
                    IsBuy = true,
                    Quantity = 10,
                    Price = 60,
                    Timestamp = new DateTime(2018, 04, 12),
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on MSFT")
                                    select inst.Id).FirstOrDefault(),
                    MarkPrice = 0,
                    PnL = 0,
                    Delta = 0,
                    Gamma = 0,
                    Vega = 0,
                    Theta = 0,
                    Rho = 0
                });
                db.Trades.Add(new Trade
                {
                    IsBuy = true,
                    Quantity = 15,
                    Price = 42,
                    Timestamp = new DateTime(2018, 04, 13),
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on AMZN")
                                    select inst.Id).FirstOrDefault(),
                    MarkPrice = 0,
                    PnL = 0,
                    Delta = 0,
                    Gamma = 0,
                    Vega = 0,
                    Theta = 0,
                    Rho = 0
                });
                db.Trades.Add(new Trade
                {
                    IsBuy = false,
                    Quantity = 2,
                    Price = 0,
                    Timestamp = new DateTime(2018, 04, 17),
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Call European Option on MSFT")
                                    select inst.Id).FirstOrDefault(),
                    MarkPrice = 0,
                    PnL = 0,
                    Delta = 0,
                    Gamma = 0,
                    Vega = 0,
                    Theta = 0,
                    Rho = 0
                });
                db.Trades.Add(new Trade
                {
                    IsBuy = true,
                    Quantity = 5,
                    Price = 0,
                    Timestamp = new DateTime(2018, 04, 18),
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Put Barrier Option on AMZN")
                                    select inst.Id).FirstOrDefault(),
                    MarkPrice = 0,
                    PnL = 0,
                    Delta = 0,
                    Gamma = 0,
                    Vega = 0,
                    Theta = 0,
                    Rho = 0
                });
                db.Trades.Add(new Trade
                {
                    IsBuy = false,
                    Quantity = 8,
                    Price = 0,
                    Timestamp = new DateTime(2018, 04, 02),
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Put Digital Option on APPL")
                                    select inst.Id).FirstOrDefault(),
                    MarkPrice = 0,
                    PnL = 0,
                    Delta = 0,
                    Gamma = 0,
                    Vega = 0,
                    Theta = 0,
                    Rho = 0
                });
                db.SaveChanges();
                Console.WriteLine("Adding Trades Succeeds!");


                db.HistoryPrices.Add(new HistoryPrice
                {
                    Date = new DateTime(2017, 03, 16),
                    ClosingPrice = 41,
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on MSFT")
                                    select inst.Id).FirstOrDefault()
                });
                db.HistoryPrices.Add(new HistoryPrice
                {
                    Date = new DateTime(2017, 04, 08),
                    ClosingPrice = 58,
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on MSFT")
                                    select inst.Id).FirstOrDefault()
                });
                db.HistoryPrices.Add(new HistoryPrice
                {
                    Date = new DateTime(2017, 02, 16),
                    ClosingPrice = 47,
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on AMZN")
                                    select inst.Id).FirstOrDefault()
                });
                db.HistoryPrices.Add(new HistoryPrice
                {
                    Date = new DateTime(2017, 04, 04),
                    ClosingPrice = 70,
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on AMZN")
                                    select inst.Id).FirstOrDefault()
                });
                db.HistoryPrices.Add(new HistoryPrice
                {
                    Date = new DateTime(2017, 04, 07),
                    ClosingPrice = 48,
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on APPL")
                                    select inst.Id).FirstOrDefault()
                });
                db.HistoryPrices.Add(new HistoryPrice
                {
                    Date = new DateTime(2017, 04, 17),
                    ClosingPrice = 67,
                    InstrumentId = (from inst in db.Instruments
                                    where inst.OptionTypeName.Contains("Stock on APPL")
                                    select inst.Id).FirstOrDefault()
                });
                db.SaveChanges();
                Console.WriteLine("Adding HistoryPrices Succeeds!");


                db.InterestRates.Add(new InterestRate
                {
                    Tenor = 1,
                    Rate = 0.02
                });
                db.InterestRates.Add(new InterestRate
                {
                    Tenor = 2,
                    Rate = 0.035
                });
                db.InterestRates.Add(new InterestRate
                {
                    Tenor = 6,
                    Rate = 0.05
                });
                db.SaveChanges();
                Console.WriteLine("Adding InsterestRates Succeeds!");

                Console.WriteLine();
                Console.WriteLine("Populating data succeeds!");
                Console.ReadLine();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
