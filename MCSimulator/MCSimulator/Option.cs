﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSimulator
{
    /// <summary>
    /// This class serves as a base class for all various types of options
    /// </summary>
    public abstract class Option
    {
        /// <summary>
        /// Calculate all outputs one by one
        /// </summary>
        public void Scheduler()
        {
            double[,] rdn = RandomMatrix.RandomMat();

            GetPrice(rdn);

            //StdError(rdn);

            Delta(rdn);

            Gamma(rdn);

            Vega(rdn);

            Theta(rdn);

            Rho(rdn);

        }

        public virtual double GetPrice(double[,] rdn)
        {
            return 0d;
        }

        public virtual double StdError(double[,] rdn)
        {
            return 0d;
        }


        /// <summary>
        /// Calculate delta
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public double Delta(double[,] rdn)
        {
            //price with underlying+.001
            IO.Underlying += 0.015;
            double price_p = GetPrice(rdn);

            //price with underlying-.001
            IO.Underlying -= 0.03;
            double price_m = GetPrice(rdn);

            IO.Delta = (price_p - price_m) / 2 / 0.015; //Calculate delta

            IO.Underlying += 0.015; //Restore to the original value

            return IO.Delta;
        }

        /// <summary>
        /// Calculate gamma
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public double Gamma(double[,] rdn)
        {
            //price with underlying+.001
            IO.Underlying += 0.015;
            double delta_p = Delta(rdn);

            //price with underlying-.001
            IO.Underlying -= 0.03;
            double delta_m = Delta(rdn);

            IO.Gamma = (delta_p - delta_m) / 2 / 0.015; //Calculate gamma

            IO.Underlying += 0.015; //Restore to the original value

            return IO.Gamma;
        }

        /// <summary>
        /// Calculate vega
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public double Vega(double[,] rdn)
        {
            //price with Vol+0.001
            IO.Vol += 0.015;
            double price_p = GetPrice(rdn);

            //price with Vol-0.001
            IO.Vol -= 0.03;
            double price_m = GetPrice(rdn);

            IO.Vega = (price_p - price_m) / 2 / 0.015; //Calculate delta

            IO.Vol += 0.015; //Restore to the original value

            return IO.Vega;
        }

        /// <summary>
        /// Calculate theta
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public double Theta(double[,] rdn)
        {
            //price with Tenor+0.001
            IO.Tenor += 0.015;
            double price_p = GetPrice(rdn);

            IO.Tenor -= 0.015; //Restore to the original value
            double price = GetPrice(rdn);

            IO.Theta = (price_p - price) / 0.015; //Calculate delta

            return IO.Theta;
        }

        /// <summary>
        /// Calculate rho
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public double Rho(double[,] rdn)
        {
            //price with R+0.001
            IO.R += 0.015;
            double price_p = GetPrice(rdn);

            //price with R-0.001
            IO.R -= 0.03;
            double price_m = GetPrice(rdn);

            IO.Rho = (price_p - price_m) / 2 / 0.015; //Calculate delta

            IO.R += 0.015; //Restore to the original value

            return IO.Rho;
        }


        /// <summary>
        /// Implement the normal CDF
        /// Ref: https://jamesmccaffrey.wordpress.com/2014/07/16/the-normal-cumulative-density-function-using-c/
        /// Ref: “Handbook of Mathematical Functions” (1965), by Abramowitz and Stegun
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        protected static double NormCDF(double z)
        {
            double p = 0.3275911;
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;

            int sign;
            if (z < 0.0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(z) / Math.Sqrt(2.0);
            double t = 1.0 / (1.0 + p * x);
            double erf = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * erf);
        }

        /// <summary>
        /// Calculate Delta in the Black-Scholes formula
        /// </summary>
        /// <param name="BS_S"></param>
        /// <param name="BS_K"></param>
        /// <param name="BS_r"></param>
        /// <param name="BS_T"></param>
        /// <param name="BS_vol"></param>
        /// <returns></returns>
        protected double BS_delta(double BS_S, double BS_K, double BS_r, double BS_T, double BS_vol)
        {
            double d1 = (Math.Log(BS_S / BS_K) + (BS_r + BS_vol * BS_vol / 2) * BS_T) / BS_vol / Math.Sqrt(BS_T);
            if (IO.Call_Put == 1) // call option
            {
                return NormCDF(d1);
            }
            else // put option
            {
                return NormCDF(d1) - 1;
            }
        }
    }
}
