﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSimulator
{
    public static class RandomMatrix
    {
        /// <summary>
        /// Generate a random matrix, where each scalar (except the first column) is a normally dist. random number
        /// </summary>
        /// <returns></returns>
        public static double[,] RandomMat()
        {
            double[,] RMat = new double[IO.NSims, IO.NSteps + 1];

            if (IO.IsAnti) // Use antithetic variance reduction...
            {
                // Parallel trials into threads (can be IO.cores or 1)
                Parallel.ForEach(IEnum.Step(0, IO.NSims / 2, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, i =>
                {
                    RMat[i, 0] = 1;
                    RMat[i + IO.NSims / 2, 0] = 1;

                    for (int j = 1; j <= IO.NSteps; j++)
                    {
                        RMat[i, j] = SingleRandomNum();
                        RMat[i + IO.NSims / 2, j] = -RMat[i, j]; // anti-correlated path
                    }

                });
            }

            else // not use antithetic
            {
                Parallel.ForEach(IEnum.Step(0, IO.NSims, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, i =>
                {
                    RMat[i, 0] = 1;

                    for (int j = 1; j <= IO.NSteps; j++)
                    {
                        RMat[i, j] = SingleRandomNum();
                    }
                });
            }
            return RMat;
        }


        static Random rdn = new Random();

        /// <summary>
        /// Generate a standard normally distributed random number using the polar rejection method
        /// </summary>
        private static double SingleRandomNum()
        {
            double w, x1, x2;

            //generate two uniform random values until w is no larger than 1
            do
            {
                lock (rdn) x1 = 2 * rdn.NextDouble() - 1;
                lock (rdn) x2 = 2 * rdn.NextDouble() - 1;
                w = x1 * x1 + x2 * x2;
            }
            while (w > 1);

            return Math.Sqrt(-2 * Math.Log(w) / w) * x1;
        }
    }
}
