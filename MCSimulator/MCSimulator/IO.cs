﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSimulator
{
    /// <summary>
    /// IO is used to store all inputs and outputs
    /// </summary>
    public static class IO
    {
        //Inputs
        static double underlying, vol, r, tenor, strike;
        static long nSims;
        static int nSteps;
        static double call_Put;
        static bool isAnti, isCv, isMultiThread;
        static int cpu_cores = 1;
        static int thread = 1;

        static double barrier;
        static int barrier_type = 1;

        static double rebate;

        //Outputs
        static double price;
        static double stdError;
        static double delta;
        static double gamma;
        static double vega;
        static double theta;
        static double rho;

        //Make properties of the private variables
        public static double Underlying { get => underlying; set => underlying = value; }
        public static double Vol { get => vol; set => vol = value; }
        public static double R { get => r; set => r = value; }
        public static double Tenor { get => tenor; set => tenor = value; }
        public static int NSteps { get => nSteps; set => nSteps = value; }
        public static long NSims { get => nSims; set => nSims = value; }
        public static double Call_Put { get => call_Put; set => call_Put = value; }
        public static double Strike { get => strike; set => strike = value; }
        public static bool IsAnti { get => isAnti; set => isAnti = value; }
        public static bool IsCv { get => isCv; set => isCv = value; }
        public static bool IsMultiThread { get => isMultiThread; set => isMultiThread = value; }

        public static double Price { get => price; set => price = value; }
        public static double Delta { get => delta; set => delta = value; }
        public static double Gamma { get => gamma; set => gamma = value; }
        public static double Vega { get => vega; set => vega = value; }
        public static double Theta { get => theta; set => theta = value; }
        public static double Rho { get => rho; set => rho = value; }
        public static double StdError { get => stdError; set => stdError = value; }

        public static int Cpu_cores { get => cpu_cores; set => cpu_cores = value; }
        public static int Thread { get => thread; set => thread = value; }

        public static double Barrier { get => barrier; set => barrier = value; }
        public static int Barrier_Type { get => barrier_type; set => barrier_type = value; }
        public static double Rebate { get => rebate; set => rebate = value; }
    }
}
