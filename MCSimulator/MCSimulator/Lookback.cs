﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSimulator
{
    public sealed class Lookback : Option
    {
        Simulator sim = new Simulator(); // Instantiate a 'Simulator' instance

        // Global variables used both in 'GetPrice' and 'StdError' methods
        static double[] payoffs_b; // Yi(b)
        static double sum_payoffs; // sum of Yi(b)
        static double sum_payoffs2; // sum of Yi(b)^2

        static double[] max_S, min_S;

        /// <summary>
        /// Calculate the European option price
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public override double GetPrice(double[,] rdn)
        {
            double[,] path = sim.GetPath(rdn); // Instantiate a path

            // set default values at the start of GetPrice called
            payoffs_b = new double[IO.NSims];
            sum_payoffs = 0;
            sum_payoffs2 = 0;

            max_S = new double[IO.NSims];
            min_S = new double[IO.NSims];

            for (long i = 0; i <= IO.NSims - 1; i++)
            {
                max_S[i] = Enumerable.Range(1, IO.NSteps).Max(j => path[i, j]);
                min_S[i] = Enumerable.Range(1, IO.NSteps).Min(j => path[i, j]);
            }

            /*---------------------------------------------------------------
             Three situations:
             1-not use Delta-based Control variate (no var redx / only anti)
             2-only use Delta-based Control variate
             3-use both Delta-based Control variate and Anthithetic
             ---------------------------------------------------------------*/
            // 1--------not use Delta-based Control variate
            if (!IO.IsCv)
            {
                double sum = 0;

                for (long i = 0; i <= IO.NSims - 1; i++) // get the final underlying price in each simulation i
                {
                    sum += (IO.Call_Put == 1) ? Math.Max(0, IO.Call_Put * (max_S[i] - IO.Strike)) : Math.Max(0, IO.Strike - min_S[i]); // Add up the option price in each simulation
                }

                //Discount back to get the option price expectation
                IO.Price = Math.Exp(-IO.R * IO.Tenor) * (sum / IO.NSims);
            }

            // 2--------only use Delta-based Control variate
            else if (IO.IsCv && !IO.IsAnti)
            {
                double[] payoffs = new double[IO.NSims]; // Yi
                double beta1 = -1;

                for (long i = 0; i <= IO.NSims - 1; i++) // each trial i...
                {
                    double cv = 0; // clear cv at time 0

                    for (int j = 1; j <= IO.NSteps; j++) // at each time step...
                    {
                        double t = (j - 1) * (IO.Tenor / IO.NSteps);
                        double delta = BS_delta(path[i, j - 1], IO.Strike, IO.R, IO.Tenor - t, IO.Vol);
                        cv += delta * (path[i, j] - path[i, j - 1] * Math.Exp(IO.R * IO.Tenor / IO.NSteps));
                    }

                    payoffs[i] = (IO.Call_Put == 1) ? Math.Max(0, IO.Call_Put * (max_S[i] - IO.Strike)) : Math.Max(0, IO.Strike - min_S[i]); // Yi
                    payoffs_b[i] = payoffs[i] + beta1 * cv; // Yi(b)=Yi+beta1*cv
                    sum_payoffs += payoffs_b[i]; // sum of Yi(b)
                    sum_payoffs2 += payoffs_b[i] * payoffs_b[i]; // sum of Yi(b)^2
                }

                IO.Price = Math.Exp(-IO.R * IO.Tenor) * (sum_payoffs / IO.NSims);

            }

            // 3--------use both Delta-based Control variate and Anthithetic
            else if (IO.IsCv && IO.IsAnti)
            {
                double[] payoffs = new double[IO.NSims]; // Yi
                double beta1 = -1;

                for (long i = 0; i <= IO.NSims / 2 - 1; i++) // each trial i... (Total HALF the trials)
                {
                    double cv1 = 0; // clear cv at time 0
                    double cv2 = 0;

                    for (int j = 1; j <= IO.NSteps; j++) // at each time step...
                    {
                        double t = (j - 1) * (IO.Tenor / IO.NSteps);
                        double delta1 = BS_delta(path[i, j - 1], IO.Strike, IO.R, IO.Tenor - t, IO.Vol);
                        double delta2 = BS_delta(path[i + IO.NSims / 2, j - 1], IO.Strike, IO.R, IO.Tenor - t, IO.Vol);
                        cv1 += delta1 * (path[i, j] - path[i, j - 1] * Math.Exp(IO.R * IO.Tenor / IO.NSteps));
                        cv2 += delta2 * (path[i + IO.NSims / 2, j] - path[i + IO.NSims / 2, j - 1] * Math.Exp(IO.R * IO.Tenor / IO.NSteps));
                    }

                    payoffs[i] = (IO.Call_Put == 1) ? Math.Max(0, IO.Call_Put * (max_S[i] - IO.Strike)) : Math.Max(0, IO.Strike - min_S[i]); // Yi
                    payoffs[i + IO.NSims / 2] = (IO.Call_Put == 1) ? Math.Max(0, IO.Call_Put * (max_S[i + IO.NSims / 2] - IO.Strike)) : Math.Max(0, IO.Strike - min_S[i + IO.NSims / 2]); // Yi

                    payoffs_b[i] = 0.5 * (payoffs[i] + beta1 * cv1 + payoffs[i + IO.NSims / 2] + beta1 * cv2); // Yi(b)=Yi+beta1*cv
                    sum_payoffs += payoffs_b[i]; // sum of Yi(b)
                    sum_payoffs2 += payoffs_b[i] * payoffs_b[i]; // sum of Yi(b)^2
                }

                IO.Price = Math.Exp(-IO.R * IO.Tenor) * (sum_payoffs / (IO.NSims / 2));
            }

            return IO.Price;
        }

        /// <summary>
        /// Calculate the standard error of the estimated option price
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public override double StdError(double[,] rdn)
        {
            double sum = 0;
            double[,] path = sim.GetPath(rdn);

            double price = GetPrice(rdn); // get the option price

            double future_price = 0; // (undiscounted) future option price

            /*---------------------------------------------------------------
             Four situations:
             1-not use variance reduction
             2-only use only use Antithetic
             3-only use Delta-based Control variate
             4-use both Delta-based Control variate and Anthithetic
             ---------------------------------------------------------------*/
            // 1--------not use variance reduction
            if (!IO.IsAnti && !IO.IsCv)
            {
                for (int i = 0; i <= IO.NSims - 1; i++) // for each simulation...
                {
                    future_price = (IO.Call_Put == 1) ? Math.Max(0, IO.Call_Put * (max_S[i] - IO.Strike)) : Math.Max(0, IO.Strike - min_S[i]);
                    sum += Math.Pow(future_price * Math.Exp(-IO.R * IO.Tenor) - price, 2);
                }

                IO.StdError = Math.Sqrt(sum / IO.NSims / (IO.NSims - 1));
            }

            // 2--------only use Antithetic
            else if (IO.IsAnti && !IO.IsCv)
            {
                double pair_avg, pair_future_price = 0;

                for (int i = 0; i <= IO.NSims / 2 - 1; i++) // (Total HALF the trials)
                {
                    future_price = (IO.Call_Put == 1) ? Math.Max(0, IO.Call_Put * (max_S[i] - IO.Strike)) : Math.Max(0, IO.Strike - min_S[i]); // generated path
                    pair_future_price = (IO.Call_Put == 1) ? Math.Max(0, IO.Call_Put * (max_S[i + IO.NSims / 2] - IO.Strike)) : Math.Max(0, IO.Strike - min_S[i + IO.NSims / 2]); // anti-path

                    pair_avg = (future_price + pair_future_price) / 2;
                    sum += Math.Pow(pair_avg * Math.Exp(-IO.R * IO.Tenor) - price, 2);

                }

                IO.StdError = Math.Sqrt(sum / (IO.NSims / 2) / (IO.NSims / 2 - 1));

            }

            // 3--------only use Delta-based Control variate
            else if (!IO.IsAnti && IO.IsCv)
            {
                IO.StdError = Math.Sqrt((sum_payoffs2 - sum_payoffs * sum_payoffs / IO.NSims) * Math.Exp(-2 * IO.R * IO.Tenor) / (IO.NSims - 1) / IO.NSims);
            }

            // 4--------use both Delta-based Control variate and Anthithetic
            else if (IO.IsAnti && IO.IsCv)
            {
                IO.StdError = Math.Sqrt((sum_payoffs2 - sum_payoffs * sum_payoffs / (IO.NSims / 2)) * Math.Exp(-2 * IO.R * IO.Tenor) / ((IO.NSims / 2) - 1) / (IO.NSims / 2));
            }

            return IO.StdError;
        }
    }
}
