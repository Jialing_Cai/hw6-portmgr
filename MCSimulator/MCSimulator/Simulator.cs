﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace MCSimulator
{
    /// <summary>
    /// Simulator is to generate a path, using a random matrix as the input
    /// </summary>
    public class Simulator
    {
        /// <summary>
        /// Generate a path (future underlying price) given a set of inputs
        /// </summary>
        /// <param name="rdn"></param>
        /// <returns></returns>
        public double[,] GetPath(double[,] rdn)
        {
            double[,] path = new double[IO.NSims, IO.NSteps + 1];

            // Parallel trials into threads (can be IO.cores or 1)
            Parallel.ForEach(IEnum.Step(0, IO.NSims, 1), new ParallelOptions { MaxDegreeOfParallelism = IO.Thread }, i =>
            {
                path[i, 0] = IO.Underlying; // set first column (time step=0) equal the underlying price

                for (int j = 1; j <= IO.NSteps; j++) // j is the step
                {
                    // calculate the underlying price at step j in the i(th) simulation
                    path[i, j] = path[i, j - 1] * Math.Exp((IO.R - 1.0 / 2 * IO.Vol * IO.Vol) * (IO.Tenor / IO.NSteps) + IO.Vol * Math.Sqrt(IO.Tenor / IO.NSteps) * rdn[i, j]);
                }
            });

            return path;
        }
    }
}
