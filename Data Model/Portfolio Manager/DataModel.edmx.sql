
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/24/2018 04:10:32
-- Generated from EDMX file: C:\Users\jacqu\Desktop\save\Data Model\Portfolio Manager\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [PortfolioManager];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'InstTypes'
CREATE TABLE [dbo].[InstTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TypeName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Instruments'
CREATE TABLE [dbo].[Instruments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Ticker] nvarchar(max)  NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [Exchange] nvarchar(max)  NOT NULL,
    [OptionTypeName] nvarchar(max)  NOT NULL,
    [IsCall] bit  NOT NULL,
    [Tenor] float  NOT NULL,
    [Strike] float  NOT NULL,
    [Rebate] float  NOT NULL,
    [Barrier] float  NOT NULL,
    [BarrierTypeName] nvarchar(max)  NOT NULL,
    [InstTypeId] int  NOT NULL
);
GO

-- Creating table 'InterestRates'
CREATE TABLE [dbo].[InterestRates] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tenor] float  NOT NULL,
    [Rate] float  NOT NULL
);
GO

-- Creating table 'HistoryPrices'
CREATE TABLE [dbo].[HistoryPrices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [ClosingPrice] float  NOT NULL,
    [InstrumentId] int  NOT NULL
);
GO

-- Creating table 'Trades'
CREATE TABLE [dbo].[Trades] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsBuy] bit  NOT NULL,
    [Quantity] int  NOT NULL,
    [Price] float  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [InstrumentId] int  NOT NULL,
    [MarkPrice] float  NOT NULL,
    [PnL] float  NOT NULL,
    [Delta] float  NOT NULL,
    [Gamma] float  NOT NULL,
    [Vega] float  NOT NULL,
    [Theta] float  NOT NULL,
    [Rho] float  NOT NULL
);
GO

-- Creating table 'Totals'
CREATE TABLE [dbo].[Totals] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TotalPnL] float  NOT NULL,
    [TotalDelta] float  NOT NULL,
    [TotalGamma] float  NOT NULL,
    [TotalVega] float  NOT NULL,
    [TotalTheta] float  NOT NULL,
    [TotalRho] float  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'InstTypes'
ALTER TABLE [dbo].[InstTypes]
ADD CONSTRAINT [PK_InstTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [PK_Instruments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InterestRates'
ALTER TABLE [dbo].[InterestRates]
ADD CONSTRAINT [PK_InterestRates]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistoryPrices'
ALTER TABLE [dbo].[HistoryPrices]
ADD CONSTRAINT [PK_HistoryPrices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Trades'
ALTER TABLE [dbo].[Trades]
ADD CONSTRAINT [PK_Trades]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Totals'
ALTER TABLE [dbo].[Totals]
ADD CONSTRAINT [PK_Totals]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [InstTypeId] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [FK_InstTypeStock]
    FOREIGN KEY ([InstTypeId])
    REFERENCES [dbo].[InstTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstTypeStock'
CREATE INDEX [IX_FK_InstTypeStock]
ON [dbo].[Instruments]
    ([InstTypeId]);
GO

-- Creating foreign key on [InstrumentId] in table 'HistoryPrices'
ALTER TABLE [dbo].[HistoryPrices]
ADD CONSTRAINT [FK_InstrumentHistoryPrice]
    FOREIGN KEY ([InstrumentId])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentHistoryPrice'
CREATE INDEX [IX_FK_InstrumentHistoryPrice]
ON [dbo].[HistoryPrices]
    ([InstrumentId]);
GO

-- Creating foreign key on [InstrumentId] in table 'Trades'
ALTER TABLE [dbo].[Trades]
ADD CONSTRAINT [FK_InstrumentTrade]
    FOREIGN KEY ([InstrumentId])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentTrade'
CREATE INDEX [IX_FK_InstrumentTrade]
ON [dbo].[Trades]
    ([InstrumentId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------